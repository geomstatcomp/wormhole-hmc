% clear
% close all
% addpath('drawdata');
addpath('utils');
dbstop if error

D = [10 20 40 100];
nD= length(D);
Algo = {'drmc','lmcwh'};
nAlgo = length(Algo);
colors = distinguishable_colors(nAlgo);
Seed = 2013:2022;
nSeed = length(Seed);

files = dir('./00_log_finished/');
nfiles = length(files) - 2;

mc = cell(nAlgo,nD,nSeed);
for ss=1:nSeed
    for dd=1:nD
        for Alg=1:nAlgo
            for ff=1:nfiles
                if ~isempty(strfind(files(ff+2).name,Algo{Alg}))&...
                        ~isempty(strfind(files(ff+2).name,strcat('sd',num2str(Seed(ss)),'_d',num2str(D(dd)),'_k10')))
                    mc{Alg,dd,ss} = load(strcat('./00_log_finished/', files(ff+2).name));
                end
            end
        end
    end
end


%% D-R plot
cuttime = 500;
cuttimeR = zeros(nAlgo,nD,nSeed);
cuttimeREM = zeros(nAlgo,nD,nSeed);
iters = zeros(nAlgo,nD,nSeed);

for dd = 1:nD
    for Alg = 1:nAlgo
        for ss=1:nSeed
            ixt = sum(mc{Alg,dd,ss}.TIME < cuttime);
            if ixt > mc{Alg,dd,ss}.p
                   ixt = mc{Alg,dd,ss}.p;
            end
            cuttimeR(Alg,dd,ss) = mc{Alg,dd,ss}.R(ixt); 
            cuttimeREM(Alg,dd,ss) = mc{Alg,dd,ss}.REMU(ixt);
            iters(Alg,dd,ss) = mc{Alg,dd,ss}.s;
        end
    end
end
R.mean = mean(cuttimeR,3); R.std = std(cuttimeR,0,3);
REM.mean = mean(cuttimeREM,3); REM.std = std(cuttimeREM,0,3);

styles = {'-o','--+','-s','--x'};

% fig.DREM = figure(404); clf;
% % set(fig.DREM,'windowstyle','docked');
% for Alg = 1:nAlgo
%     errorbar(1:nD,REM.mean(Alg,:),REM.std(Alg,:)./sqrt(nSeed),styles{Alg},'color',colors(Alg,:),'linewidth',2,'markersize',7); hold on;
% end
% set(gca,'FontSize',15);
% set(gca,'XTick',1:nD,'XTickLabel','10|20|40|100');
% xlabel('D','FontSize',18); ylabel('REM (after 500 sec)','FontSize',18); 
% legend('RDMC','WHMC','FontSize',18,'location','NorthWest');
% title('K=10','FontSize',20);


fig.DR = figure(403); clf;
% set(fig.DR,'windowstyle','docked');
% for Alg = 1:nAlgo
%     errorbar(1:nD,R.mean(Alg,:),R.std(Alg,:),styles{Alg},'color',colors(Alg,:),'linewidth',2,'markersize',10); hold on;
% end
% ylim([1 2.2]);

R_recal = zeros(nAlgo,nD);
for dd = 1:nD
    for Alg = 1:nAlgo
        sampidx = min(iters(Alg,dd,:));
        cmb_SAMP = [];
        for ss=1:nSeed
            if Alg==1
                cmb_SAMP(:,:,ss) = mc{Alg,dd,ss}.SAMP{1}(:,1:sampidx)';
            else
                cmb_SAMP(:,:,ss) = mc{Alg,dd,ss}.SAMP(:,1:sampidx)';
            end
        end
        R_recal(Alg,dd) = mpsrf(cmb_SAMP);
    end
end

for Alg = 1:nAlgo
    plot(1:nD,R_recal(Alg,:),styles{Alg},'color',colors(Alg,:),'linewidth',2,'markersize',7); hold on;
end
xlim([.5,nD+.5]);
ylim([1-.05,1.8]);
set(gca,'FontSize',15);
set(gca,'XTick',1:nD,'XTickLabel','10|20|40|100');
xlabel('D','FontSize',18); ylabel('R (after 500 sec)','FontSize',18);
legend('RDMC','WHMC','FontSize',18,'location','NorthWest');
title('K=10','FontSize',20);


