% clear
% close all
% addpath('drawdata');
addpath('utils');
dbstop if error

D = [10 20 40 100];
nD= 4;
Algo = {'drmc','lmcwh'};
nAlgo = 2;
colors = distinguishable_colors(nAlgo);

files = dir('./dataD');
nfiles = length(files) - 2;

mc = cell(1,nAlgo*nD);
for dd=1:nD
    for Alg=1:nAlgo
        for i=1:nfiles
            if ~isempty(strfind(files(i+2).name,Algo{Alg}))&~isempty(strfind(files(i+2).name,strcat('d',num2str(D(dd)))))
                mc{(dd-1)*nAlgo+Alg} = load(strcat('./dataD/', files(i+2).name));
            end
        end
    end
end


%% D-R plot
cuttime = 400;
cuttimeR = zeros(nD*nAlgo,1);
cuttimeREM = zeros(nD*nAlgo,1);

for dd = 1:nD
    for i = 1:nAlgo
            ii = (dd-1)*nAlgo + i;
            ixt = sum(mc{ii}.TIME(1:mc{ii}.p) < cuttime);
            if ixt > mc{ii}.p
                   ixt = mc{ii}.p;
            end
            cuttimeR(ii) = mc{ii}.R(ixt); 
            cuttimeREM(ii) = mc{ii}.REMU(ixt);
    end
end

styles = {'-o','--+','-s','--x'};

% fig.DR = figure(403); clf;
% set(fig.DR,'windowstyle','docked');
% for i = 1:nAlgo
%     plot(1:nD,cuttimeR(i:nAlgo:end)',styles{i},'color',colors(i,:),'linewidth',2,'markersize',10); hold on;
% end
% ylim([1 2.2]);
% set(gca,'FontSize',15);
% set(gca,'XTick',1:nD,'XTickLabel','10|20|40|100');
% xlabel('Dimension','FontSize',18); ylabel('R at time = 400 sec ','FontSize',18);
% legend('RDMC','WORMHOLE','FontSize',18,'location','NorthWest');
% title('R vs Dimension (K=10)','FontSize',20);

fig.DREM = figure(404); clf;
set(fig.DREM,'windowstyle','docked');
for i = 1:nAlgo
    plot(1:nD,cuttimeREM(i:nAlgo:end)',styles{i},'color',colors(i,:),'linewidth',2,'markersize',10); hold on;
end
set(gca,'FontSize',15);
set(gca,'XTick',1:nD,'XTickLabel','10|20|40|100');
xlabel('D','FontSize',18); ylabel('REM (after 400 sec)','FontSize',18); 
legend('RDMC','WHMC','FontSize',18,'location','NorthWest');
title('K=10','FontSize',20);

