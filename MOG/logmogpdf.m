function logdensity = logmogpdf(x,mu,cv,pik)

[~, k] = size(mu);
n = size(x,1);

logpk = zeros(n,k);
for i=1:k
    logpk(:,i) = logmvnpdf(x,mu(:,i)',cv(:,:,i));
end

logpk = logpk + repmat(log(pik),n,1);
logdensity = logsumexp(logpk,2);

 