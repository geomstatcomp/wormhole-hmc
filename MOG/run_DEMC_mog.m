function run_DEMC_mog(seed,dim,iterburnin,...
                                     mxit,itvplot,itvprint,itvsave,PLOT,mog,nparallel,stepsz,noise,npopu,maxruntime)

% clear
% close all
dbstop if error
format compact
addpath('../package/vdpgm');
addpath('../00_drmcmc_core');
addpath('../00_utils');

if isdeployed
        
        seed = str2double(seed);
        dim = str2double(dim);
        iterburnin = str2double(iterburnin); 
        mxit = str2double(mxit); 
        itvplot = str2double(itvplot);
        itvsave = str2double(itvsave);
        itvprint = str2double(itvprint);
        PLOT = str2double(PLOT);
        nparallel = str2double(nparallel);
        stepsz = str2double(stepsz);
        noise = str2double(noise);        
        npopu = str2double(npopu);        
        maxruntime = str2double(maxruntime);        
        
end

dosubdimupdate = 0;
dock = 1;

%% random stream
% RandStream.setDefaultStream(RandStream('mt19937ar','Seed',seed));
RandStream.setGlobalStream(RandStream('mt19937ar','Seed',seed));


%% Init Parallelization
sz.pool = matlabpool('size');

if sz.pool > 0 && sz.pool ~= nparallel && nparallel ~= 0
        
        matlabpool close
        matlabpool(nparallel);        
        
end

if sz.pool == 0 && nparallel ~= 0
        
        matlabpool(nparallel);
        
end

maxNumCompThreads(1);

%% get function handles
[llhfunc,gradfunc] = funcslogmog(mog);
% clear ld


%% fname
fname = sprintf('diffevol_mog_para_sd%d_d%d_k%d_popu%d_para%d_rt%d', seed, dim, mog.k, npopu,nparallel,maxruntime);
fname = strrep(fname,'.','-');
fname = appendfilehead(fname);
fname = strcat('_',fname);


%% plots
nhist = 1;
for j = 1:nhist
        fig.hist3(j) = figure(730+j); clf;
end

fig.rerror_mu = figure(750); clf;
% fig.rerror_cov = figure(751); clf;
fig.R = figure(752); clf;
% fig.locerr = figure(753); clf;

if dock && PLOT && ~isdeployed
        
        for j = 1:nhist
                set(fig.hist3(j),'windowstyle','docked');
        end
        set(fig.rerror_mu,'windowstyle','docked');   
%         set(fig.rerror_cov,'windowstyle','docked');   
        set(fig.R,'windowstyle','docked');   
%         set(fig.locerr,'windowstyle','docked');
        
end

pause(0.5);
figrow = 1;
% nfig = figrow^2;
nfig = 1;
nfig = min(nfig,dim-1);
% cvpair = [1:nfig; mod((1:nfig) + dim/2 -1, dim)+1]';
cvpair = [1 2];
colors = distinguishable_colors(mog.k);

%% draw components
for qq = 1:nhist
        
        set(0,'CurrentFigure',fig.hist3(qq));  colormap default
        linewidth = 1;

        for f=1:nfig

                subplot(figrow,figrow,f);        
                hold on;
                ii = cvpair(f,1);
                jj = cvpair(f,2);

                for kk=1:mog.k

                        plotGauss(mog.mu(ii,kk),mog.mu(jj,kk),...
                                         mog.cv(ii,ii,kk),mog.cv(jj,jj,kk),mog.cv(ii,jj,kk),...
                                         2,'--r',linewidth); hold on;  
%                         plot(mog.mu(ii,kk),mog.mu(jj,kk),'color',colors(kk,:),'marker','x','markersize',10,'linewidth',2);            
                        plot(mog.mu(ii,kk),mog.mu(jj,kk),'k+','markersize',10,'linewidth',2);            
                end
                
                axis tight
                axax(f,:) = axis;

        end
        
end

drawnow;

%% draw mog hist3(qq)
ax = max(abs(axax),[],1);
ax([1 3]) = -1*ax([1 3]);
rangx = linspace(ax(1),ax(2),100);
rangy = linspace(ax(3),ax(4),100);
[xx yy] = meshgrid(rangx,rangy);
xv = xx(:);
yv = yy(:);
xy = [xv yv];
prxy = zeros([size(xx)  mog.k]);

for i=1:nfig

        subplot(figrow,figrow,i); hold on;          
        ii = cvpair(i,1);
        jj = cvpair(i,2);
        density = exp(logmogpdf(xy,mog.mu([ii jj],:),mog.cv([ii,jj],[ii,jj],:),mog.pik));              
        prxy(:,:,i) = reshape(density,length(rangy),length(rangx));    
        contour(xx, yy,prxy(:,:,i),15); 
        
end

drawnow;


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Overdispersed Initilization
rangemin = min(mog.mu')*1.2;
rangemax = max(mog.mu')*1.2;        
wcurr = repmat(rangemin,npopu,1) + rand(npopu,dim).*repmat((rangemax-rangemin),npopu,1);
% wcurr = randn(dim,npopu) + repmat(mean(mog.mu,2),1,npopu);
assert(all(min(wcurr) >= rangemin) && all(max(wcurr) <= rangemax));
wcurr = wcurr';

plot(wcurr(1,:),wcurr(2,:),'m.','markersize',10,'linewidth',2);            
drawnow;

wdir = zeros(dim,npopu);
SAMP = zeros(dim, mxit, npopu);
TIME = zeros(mxit, 1);
R = zeros(ceil(mxit/itvplot)+100,1);
% RMU = zeros(ceil(mxit/itvplot)+100,1);
% RVAR = zeros(ceil(mxit/itvplot)+100,1);
REMU = zeros(ceil(mxit/itvplot)+100,1);
% RECV = zeros(ceil(mxit/itvplot)+100,1);
% ELOC = zeros(ceil(mxit/itvplot)+100,1);
avgmhratio = zeros(1, npopu);
numaccept = zeros(1, npopu);

t = 0;
p = 0;
v = 0;
s = 0;
tottime = 0;
gt.mu = mog.pik*mog.mu';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

while t < mxit && tottime < maxruntime
        
        tic
        
        t = t + 1;                  
        
        J = 1:npopu;
        K = 1:npopu;
        
        % obtain direction vectors
        for qq = 1:npopu
                
                while J(qq) == qq
                        J(qq) = randi(npopu);
                end
                
                while K(qq) == qq || K(qq) == J(qq)
                        K(qq) = randi(npopu);
                end
                
        end
        
        % parallel proposals
        wprop = zeros(dim,npopu);
        
        for qq=1:npopu

                wdir(:,qq) = wcurr(:,J(qq)) - wcurr(:,K(qq));
                
                if  t > iterburnin 
                        
                        if dosubdimupdate
                                
                                wupdate(:,qq) = zeros(dim,1);
                                ix = randi(numnode);
                                wupdate([ix ix+numnode],qq) = wdir([ix ix+numnode],qq);                
                                wprop(:,qq) = wcurr(:,qq) + stepsz*wupdate(:,qq) + sqrt(noise)*randn(dim,1);
                                
                        else
                                wprop(:,qq) = wcurr(:,qq) + stepsz*wdir(:,qq) + sqrt(noise)*randn(dim,1);
                        end
                        
                else
                        
                        wprop(:,qq) = wcurr(:,qq) + stepsz*wdir(:,qq) + sqrt(noise)*randn(dim,1);
                        
                end
                
                ratio(qq) = min(1, exp(llhfunc(wprop(:,qq)') - llhfunc(wcurr(:,qq)')));
                avgmhratio(qq) = (1-1/t)*avgmhratio(qq) + (1/t)*ratio(qq);
                accept(qq) = rand < ratio(qq);               
                
                if accept(qq)
                        wcurr(:,qq) = wprop(:,qq);
                        numaccept(qq) = numaccept(qq) + 1;
                end
        
        end
  
%         if t > iterburnin
                
        s = s + 1;
        SAMP(:,s,:) = wcurr;
        tottime = tottime + toc;
                
%         end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% save
        if ~rem(t+itvsave,itvsave) && p > 0
                
                v = v + 1;
                
                cd 00_log_working

                % PNG
%                 saveas(fig.hist3(1),strcat(fname,'_HIST1.png'),'png');
%                 saveas(fig.hist3(2),strcat(fname,'_HIST2.png'),'png');
%                 saveas(fig.R,strcat(fname,'_R.png'),'png');
%                 saveas(fig.rerror_mu,strcat(fname,'_REMU.png'),'png');
%                 saveas(fig.rerror_cov,strcat(fname,'_RECV.png'),'png');
%                 
%                 % FIG
%                 saveas(fig.hist3(1),strcat(fname,'_HIST1.fig'),'fig');
%                 saveas(fig.hist3(2),strcat(fname,'_HIST2.fig'),'fig');
%                 saveas(fig.R,strcat(fname,'_R.fig'),'fig');
%                 saveas(fig.rerror_mu,strcat(fname,'_REMU.fig'),'fig');
%                 saveas(fig.rerror_cov,strcat(fname,'_RECV.fig'),'fig');

                save(fname);
                % save(fname,'SAMP','s','seed','dim','lfn','lfsize','nrestart','ninitsamp','iterburnin','nsubsamp');          
                
                disp('---------------- SAVE COMPLETE! ----------------');           
                cd ..
                
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% print
        if ~rem(t+itvprint,itvprint)
                
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% plot            
        if ~rem(t+itvplot,itvplot)
                
                tic
                
                p = p + 1;
                meanmhratio = mean(avgmhratio)
                t
                
                % numaccept 
                
                %% R convergence Diagnostic
                if s > iterburnin
                        
                        % compute R convergence diagnostic
                        R(p) = mpsrf(permute(SAMP(:,1:s,:),[2 1 3]));                                   
                        
                                       
                        %% compute error
                        combsamp = [];
                        for qq=1:npopu                        

                                sampmu = mean(SAMP(:,1:s,qq)');
                                REMU(p,qq) = sum(abs(gt.mu - sampmu))/sum(abs(gt.mu));
                                combsamp = [combsamp SAMP(:,1:s,qq)];
                                
                        end
                        combsampmu = mean(combsamp,2);
                        REMUT(p) = sum(abs(gt.mu - combsampmu'))/sum(abs(gt.mu));
                
                        TIME(p) = tottime + toc;
                        
                        % R
                        set(0, 'currentfigure', fig.R);  
                        plot(TIME(1:p), R(1:p), '-+k'); hold on; 
                        
                        % REM
                        if p > 1
                                set(0, 'currentfigure', fig.rerror_mu); clf;
                                % plot(cumtime(itv), mean(REMU(1:p,:)'), '-r'); hold on;
                                shadedErrorBar(TIME(1:p),mean(REMU(1:p,:)'), std(REMU(1:p,:)'),...
                                        {'r-+','markerfacecolor','r'},1); hold on;
                                plot(TIME(1:p),REMUT(1:p),'-+b');
                        end
                        
                        

                                                
                end
                
                drawnow;
                
                %% plot                        
                for qq=1:nhist
                                                

%                         fprintf('%d - ntour:%d/%d: Burn-in iter: %d, accept rate: %f\n', qq, ntour, npopu, t, avgprhmcaccept(qq));
                        set(0,'CurrentFigure',fig.hist3(qq)); clf;

                        for ff = 1:nfig

                                subplot(figrow,figrow,ff);             
                                ii = cvpair(ff,1);
                                jj = cvpair(ff,2);   

%                                 [CU,HU] = hist3image(SAMP(ii,1:s,qq),SAMP(jj,1:s,qq),50);
%                                 imagesc(CU{1},CU{2},-HU); axis xy; colormap hot; hold on;            

                                contour(xx, yy,prxy(:,:,ff),10); hold on;

                                for kk = 1:mog.k

                                        plotGauss(mog.mu(ii,kk),mog.mu(jj,kk),...
                                                         mog.cv(ii,ii,kk),mog.cv(jj,jj,kk),mog.cv(ii,jj,kk),...
                                                         2,'--r',linewidth); hold on;
%                                         plot(mog.mu(ii,kk),mog.mu(jj,kk),'color',colors(kk,:),'marker','x','markersize',10,'linewidth',2);            
                                        plot(mog.mu(ii,kk),mog.mu(jj,kk),'k+','markersize',10,'linewidth',2);            

                                end

                                plot(SAMP(ii,1:s,qq),SAMP(jj,1:s,qq),'b.','markersize',1);
                                plot(wcurr(ii,qq),wcurr(jj,qq),'ro','markersize',10,'linewidth',2);       
                                plot(wcurr(1,:),wcurr(2,:),'m.','markersize',10,'linewidth',2);            
                                drawnow;
                                
                        end

                        hold off
                        axis(axax(ff,:));
                
                        title(strrep(fname,'_','-'));
                        drawnow;
                        
                end
        end
end

%% save when finished
fname = fname(2:end);
cd 00_log_finished
% 
% saveas(fig.hist3(1),strcat(fname,'_HIST1.png'),'png');
% saveas(fig.hist3(2),strcat(fname,'_HIST2.png'),'png');
% saveas(fig.R,strcat(fname,'_R.png'),'png');
% saveas(fig.rerror_mu,strcat(fname,'_REMU.png'),'png');
% saveas(fig.rerror_cov,strcat(fname,'_RECV.png'),'png');
% 
% saveas(fig.hist3(1),strcat(fname,'_HIST1.fig'),'fig');
% saveas(fig.hist3(2),strcat(fname,'_HIST2.fig'),'fig');
% saveas(fig.R,strcat(fname,'_R.fig'),'fig');
% saveas(fig.rerror_mu,strcat(fname,'_REMU.fig'),'fig');
% saveas(fig.rerror_cov,strcat(fname,'_RECV.fig'),'fig');
% 
SAMP = SAMP(:,1:s,:); 
TIME = TIME(1:s);
REMU = REMU(1:p,:);
R = R(1:p);
save(fname);
cd ..
disp('---------------- RUN COMPLETE! ----------------');           
% save(fname,'SAMP','s','seed','dim','lfnum','lfsize','nrestart','ninitsamp','iterburnin','nsubsamp');
                 