function [acprat] = sim_MOG( NumOfIterations, BurnIn )

%%%% this is the extra-dimension version-- worm hole!
%%%% this is to automatically search unknown modes via mode surgery

% addpath('~/Documents/MATLAB/lbfgsb/lbfgsb3.0_mex1.2/');
% addpath('~/Documents/MATLAB/lbfgsb-for-matlab/');

if(nargin==0)
    NumOfIterations    = 2000;
    BurnIn             = 100;
elseif(nargin==1)
    BurnIn = floor(.2*NumOfIterations);
elseif(nargin>2)
    error('wrong number of inputs!');
end

addpath('../Comparison/00_utils');
% Random Numbers...
randn('state', 2013);
rand('twister', 2013);
warning('OFF');

% data
global mog
dim = 100; k = 10; % Dimension and Components
compdist = 20;
[mu,cv,invcv] = getmog_1(k,dim,compdist); 
mog.mu = mu + 1; 
mog.cv = cv; 
mog.invcv = invcv;
mog.k = k; 
mog.dim = dim;
pik = ones(1,k);
mog.pik = pik./sum(pik);

% known modes
global mode
mode.center=mog.mu(:,1:2:end);
mode.hess=mog.cv(:,:,1:2:end);
[mode.dim, mode.k]=size(mode.center);
mode.wt=mog.pik(1:2:end);


% draw posterior
fig1=figure(1); clf;
set(fig1,'windowstyle','docked');
set(0,'CurrentFigure',fig1);
linewidth = 1;

nfig = 1;
figrow = 1;
figcol = nfig/figrow;
% cvpair = [1:nfig; mod((1:nfig) + dim/2 -1, dim)+1]';
cvpair = [1 2; 3 4; 5 6; 7 8];
colors = distinguishable_colors(mog.k);

for f=1:nfig
    
    subplot(figrow,figcol,f);        
    hold on;
    ii = cvpair(f,1);
    jj = cvpair(f,2);

    for d=1:mog.k
        plotGauss(mog.mu(ii,d),mog.mu(jj,d),...
                 mog.cv(ii,ii,d),mog.cv(jj,jj,d),mog.cv(ii,jj,d),...
                 2,'--k',linewidth); hold on;  
%         plot(mog.mu(ii,kk),mog.mu(jj,kk),'color',colors(kk,:),'marker','x','markersize',10,'linewidth',2);                        
        plot(mog.mu(ii,d),mog.mu(jj,d),'k+','markersize',10,'linewidth',2);
        text(mog.mu(ii,d)+.5,mog.mu(jj,d),num2str(d));
    end
    
    axis tight
    axax(f,:) = axis*1.2;

end
ax = max(abs(axax),[],1);
ax([1 3]) = -1*ax([1 3]);
rangx = linspace(ax(1),ax(2),100);
rangy = linspace(ax(3),ax(4),100);
[xx yy] = meshgrid(rangx,rangy);
xv = xx(:);
yv = yy(:);
xy = [xv yv];
prxy = zeros([size(xx)  mog.k]);

for i=1:nfig
    subplot(figrow,figcol,i); hold on;          
    ii = cvpair(i,1);
    jj = cvpair(i,2);
    density = exp(logmogpdf(xy,mog.mu([ii jj],:),mog.cv([ii,jj],[ii,jj],:),mog.pik));              
    prxy(:,:,i) = reshape(density,length(rangy),length(rangx));    
    contour(xx, yy,prxy(:,:,i),15); colormap default;
end

warning off
% HMC Setup
TrajectoryLength   = 1;
NumOfLeapFrogSteps = 10;
StepSize           = TrajectoryLength/NumOfLeapFrogSteps;
NewtonSteps = 5;


% define tunnel
global tunnel
% constructing super-tunnel:  network
tunnel.length = zeros(mode.k); tunnel.width = zeros(mode.k);
tunnel.upvec = cell(mode.k); tunnel.dnvec = cell(mode.k);
tunnel.upmet = cell(mode.k); tunnel.dnmet = cell(mode.k);
for i=1:mode.k
    [V,D]=eig(mode.hess(:,:,i));
    for j=i:mode.k
        tunnel.length(i,j) = norm([mode.center(:,j)-mode.center(:,i); 2]); % extra-dimension 2=1-(-1);
        upvec = [mode.center(:,j)-mode.center(:,i); 2]./tunnel.length(i,j); tunnel.upvec{i,j} = upvec;
        dnvec = upvec; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
        tunnel.width(i,j) = 2*D(end,end)*norm(V(:,end)-upvec(1:mode.dim).*(upvec(1:mode.dim)'*V(:,end)));
        tunnel.upmet{i,j} = eye(mode.dim+1)-(1-StepSize/20).*(upvec*upvec');
        tunnel.dnmet{i,j} = eye(mode.dim+1)-(1-StepSize/20).*(dnvec*dnvec');
    end
    for j=1:i-1
        tunnel.length(i,j) = tunnel.length(j,i);
        upvec = -tunnel.upvec{j,i}; upvec(end)=-upvec(end); tunnel.upvec{i,j} = upvec;
        dnvec = -tunnel.dnvec{j,i}; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
        tunnel.width(i,j) = 2*D(end,end)*norm(V(:,end)-upvec(1:mode.dim).*(upvec(1:mode.dim)'*V(:,end)));
        tunnel.upmet{i,j} = eye(mode.dim+1)-(1-StepSize/20).*(upvec*upvec');
        tunnel.dnmet{i,j} = eye(mode.dim+1)-(1-StepSize/20).*(dnvec*dnvec');
    end
end
tunnel.influence = .1;
tunnel.width = tunnel.influence.*tunnel.width;


Proposed = 0;
Accepted = 0;
NJump = 0;


global llhfunc gradfunc llhfunc_m gradfunc_m
% get function handles
[llhfunc,gradfunc] = funcslogmog(mog);
% functions from known modes
[llhfunc_m,gradfunc_m] = funcslogmode(mode);

fig2=figure(2); clf;
set(fig2,'windowstyle','docked');
set(0,'CurrentFigure',fig2);
T=1.05;

for i=1:nfig
    subplot(figrow,figcol,i); hold on;          
    ii = cvpair(i,1);
    jj = cvpair(i,2);
    densitydiff = -exp(logmogpdf(xy,mode.center([ii jj],:),mode.hess([ii,jj],[ii,jj],:),mode.wt)/T)+exp(logmogpdf(xy,mog.mu([ii jj],:),mog.cv([ii,jj],[ii,jj],:),mog.pik));              
    prxy(:,:,i) = reshape(densitydiff,length(rangy),length(rangx));    
    contour(xx, yy,prxy(:,:,i),15); colormap(winter);
end
title('Contour of modes after surgery','FontSize',20); % not a good way!

% hold off


% for i=1:nfig
%     subplot(figrow,figcol,i); hold on;          
%     ii = cvpair(i,1);
%     jj = cvpair(i,2);
%     density = exp(logmogpdf(xy,mode.center([ii jj],:),mode.hess([ii,jj],[ii,jj],:),mode.wt));              
%     prxy(:,:,i) = reshape(density,length(rangy),length(rangx));    
%     contour(xx, yy,prxy(:,:,i)); colormap(winter);
% end
% 
% hold off

set(fig1,'windowstyle','docked');
set(0,'CurrentFigure',fig1);

% Set initial values of theta
x = [randn(mode.dim,1); -1]; % adding another anciliary variable
xSaved = zeros(NumOfIterations-BurnIn,mode.dim);
zSaved = zeros(NumOfIterations-BurnIn,1);
Currentx = x;

% prep: energy
CurrentU = U(x);
dphi = U(x,1);


for IterationNum = 1:NumOfIterations
        
    if mod(IterationNum,100) == 0
        disp([num2str(IterationNum) ' iterations completed.'])
        disp(['Real time acceptance rate ', num2str(Accepted/Proposed)]);
        disp(['Accumulatd jumping rate ', num2str(NJump/IterationNum)]);
        Proposed = 0;
        Accepted = 0;
        drawnow
    end
    
    x = Currentx; xNew = x;
    dphiNew = dphi;
    
    % propose velocity
    Velocity = randn(mode.dim+1,1);
    Proposed = Proposed + 1;
    
    % Calculate current H value
    CurrentH = CurrentU + (Velocity'*Velocity)/2;
    
    % random number of leapfrog steps
%     RandomStep = ceil(rand*NumOfLeapFrogSteps);
    
    jumped = 0; EnergyAdj = 0; adjusted = 0;
    % Perform leapfrog steps
    for StepNum = 1:NumOfLeapFrogSteps
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        Velocity = Velocity- (StepSize/2)*dphiNew;
        
        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        % Make a full step for the position
        if jumped
            xNew = xNew + StepSize.*Velocity;
        else
            xTemp = xNew;
            [~,closest] = min(sum((repmat(xTemp(1:mode.dim),1,mode.k)-mode.center).^2));
            tnl.length = tunnel.length(closest,:);
            if xTemp(end)<0
                tnl.L = [mode.center(:,closest); -1]; tnl.R = [mode.center; ones(1,mode.k)];
                tnl.vec = cat(2,tunnel.upvec{closest,:});
            else
                tnl.L = [mode.center(:,closest); 1]; tnl.R = [mode.center; -ones(1,mode.k)];
                tnl.vec = cat(2,tunnel.dnvec{closest,:});
            end
            for FixedPointIteration = 1:NewtonSteps

                X2tnlL = repmat(xTemp-tnl.L,1,mode.k); X2tnlR = repmat(xTemp,1,mode.k)-tnl.R;
                projL = sum(X2tnlL.*tnl.vec); projR = sum(X2tnlR.*tnl.vec);
                vicinity = sum(X2tnlL.*X2tnlR) + abs(projL.*projR);
                moll = exp(-vicinity/mode.dim./tunnel.width(closest,:));
                if rand<1-sum(moll)
                    Move = Velocity;
                else
                    jump2 = randsample(mode.k,1,true,moll);
%                     disp(['Jumping to mode ' num2str(jump2)]);
                    Move = -2/StepSize.*X2tnlR(:,jump2);
                    jumped = 1;
                end
                if FixedPointIteration==1
                    Movefixed = Move;
                end

                xTemp = xNew + .5*StepSize.*(Movefixed+Move);
            end
            [~,closestNew] = min(sum((repmat(xTemp(1:mode.dim),1,mode.k)-mode.center).^2));
            if closestNew~=closest
                EnergyAdj = U(xTemp) - (U(xNew)+(Velocity'*Velocity)/2);
                adjusted = 1;
                NJump = NJump + 1;
            end
            xNew = xTemp;
        end
%         PropPath(StepNum) = plot([x(1),xNew(1)], [x(2),xNew(2)],'b--','LineWidth',1.5); pause(.1);
        
        
        % Update dU based on new parameters
        dphiNew = U(xNew,1);
        
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        Velocity = Velocity- (StepSize/2)*dphiNew;
        if adjusted
            EnergyAdj = EnergyAdj + (Velocity'*Velocity)/2;
            adjusted = 0;
        end
        
        x = xNew;
    end
    
    try
        % Calculate proposed H value
        ProposedU = U(xNew);
        ProposedH = ProposedU + (Velocity'*Velocity)/2;
        
        % Accept according to ratio
        Ratio = -ProposedH + CurrentH + EnergyAdj;

        if Ratio > 0 || (Ratio > log(rand))
            Currentx = xNew; CurrentU = ProposedU;
            dphi = dphiNew;
            Accepted = Accepted + 1;
%             plot(xNew(1), xNew(2),'bo','MarkerFaceColor','k'); pause(.2);
        else
%             plot(xNew(1), xNew(2),'rs','MarkerFaceColor','k'); pause(.2);
%             delete(PropPath);
        end
    catch
%         plot(xNew(1), xNew(2),'rs','MarkerFaceColor','k'); pause(.2);
%         try
%             delete(PropPath);
%         end
    end

    % Save samples if required
    if IterationNum > BurnIn
        xSaved(IterationNum-BurnIn,:) = Currentx(1:mode.dim);
        zSaved(IterationNum-BurnIn) = Currentx(end);
    end
    
    % Start timer after burn-in
    if IterationNum == BurnIn
        disp('Burn-in complete, now drawing posterior samples.')
        tic;
    end
    
    % Do mode search
    if mod(IterationNum,50) == 0
        disp('Start mode searching...');
        x_ms=Currentx(1:mode.dim)+10*randn(mode.dim,1);
        plot(x_ms(1), x_ms(2),'bo','MarkerFaceColor','k');
        
        
        % climbing down %
        options=optimoptions('fminunc','GradObj','on','MaxIter',100,'TolX',1e-10);
        
        [mode_cand,fval,exitflag,output,grad,hessian]=fminunc(@U_surg,x_ms,options);
        plot(mode_cand(1), mode_cand(2),'gs','MarkerFaceColor','k'); pause(.1);
        
        if (exitflag==1||exitflag==3)&&all(eig(hessian)>=1e-6)&&all(max(abs(mode.center-repmat(mode_cand,1,mode.k)))>1e-2)
            mode.center=[mode.center,mode_cand];
            mode.k=mode.k+1;
%             mode.hess(:,:,mode.k)=inv(hessian);
%             mode.wt=[mode.wt,.1];
            [~,which_comp]=min(max(abs(mog.mu-repmat(mode_cand,1,mog.k))));
            mode.hess=cat(3,mode.hess,mog.cv(:,:,which_comp));
            mode.wt=[mode.wt,mog.pik(which_comp)];
            
            [llhfunc_m,gradfunc_m] = funcslogmode(mode); % update the function with newly found modes
            
            disp('New mode found!');
            
            plot(mode_cand(1), mode_cand(2),'rx','MarkerSize',10,'LineWidth',2); pause(.1);
            
            % reconstruct wormhole network %
            tunnel.length = zeros(mode.k); tunnel.width = zeros(mode.k);
            tunnel.upvec = cell(mode.k); tunnel.dnvec = cell(mode.k);
            tunnel.upmet = cell(mode.k); tunnel.dnmet = cell(mode.k);
            for i=1:mode.k
                [V,D]=eig(mode.hess(:,:,i));
                for j=i:mode.k
                    tunnel.length(i,j) = norm([mode.center(:,j)-mode.center(:,i); 2]); % extra-dimension 2=1-(-1);
                    upvec = [mode.center(:,j)-mode.center(:,i); 2]./tunnel.length(i,j); tunnel.upvec{i,j} = upvec;
                    dnvec = upvec; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
                    tunnel.width(i,j) = 2*D(end,end)*norm(V(:,end)-upvec(1:mode.dim).*(upvec(1:mode.dim)'*V(:,end)));
                    tunnel.upmet{i,j} = eye(mode.dim+1)-(1-StepSize/20).*(upvec*upvec');
                    tunnel.dnmet{i,j} = eye(mode.dim+1)-(1-StepSize/20).*(dnvec*dnvec');
                end
                for j=1:i-1
                    tunnel.length(i,j) = tunnel.length(j,i);
                    upvec = -tunnel.upvec{j,i}; upvec(end)=-upvec(end); tunnel.upvec{i,j} = upvec;
                    dnvec = -tunnel.dnvec{j,i}; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
                    tunnel.width(i,j) = 2*D(end,end)*norm(V(:,end)-upvec(1:mode.dim).*(upvec(1:mode.dim)'*V(:,end)));
                    tunnel.upmet{i,j} = eye(mode.dim+1)-(1-StepSize/20).*(upvec*upvec');
                    tunnel.dnmet{i,j} = eye(mode.dim+1)-(1-StepSize/20).*(dnvec*dnvec');
                end
            end
            tunnel.width = tunnel.influence.*tunnel.width;
            
        else
            disp('Nothing found!');
        end
    end
    
end

% Stop timer
TimeTaken = toc
acprat = size(unique(xSaved,'rows'),1)/(NumOfIterations-BurnIn)

CurTime = fix(clock);
% save(['Results/sim_MOG_D' num2str(mode.cim) '_K' num2str(mode.k) '_' num2str(CurTime) '.mat'], 'xSaved','TimeTaken','NumOfLeapFrogSteps','StepSize','NewtonSteps');


% Plot paths and histograms
fig2=figure(2); clf;
set(fig2,'windowstyle','docked');

subplot(2,2,1);
plot(xSaved);
title('Trace plot','FontSize',15);
subplot(2,2,2);
scatter(xSaved(:,1),xSaved(:,2),5,'filled');
title('Scatter plot','FontSize',15);

for d=1:1
    subplot(2,2,2+d);
    [f,x]=hist(xSaved(:,d),50);
    bar(x,f/trapz(x,f));hold on
    [f,xi] = ksdensity(xSaved(:,d));
    plot(xi,f,'r');hold off
    title(['Histogram of  x_' num2str(d)],'FontSize',15);
end

subplot(2,2,4);
plot(zSaved);
title('Z plot- Extra Dim','FontSize',15);

end


%%%%%%%%%%%%% functions needed %%%%%%%%%%%%%%%%
function [output] = U(x,der)
if(nargin<2)
    der=0;
end

global mog
% phi = cellfun(@(mu,Sigma)mvnpdf(x(1:mog.dim,:)',mu,Sigma),mat2cell(mog.mu',ones(1,mog.k),mog.dim)',mat2cell(reshape(mog.cv,mog.dim,mog.dim*mog.k),mog.dim,mog.dim*ones(1,mog.k)));
% wtedphi = mog.pik.*phi;

global llhfunc gradfunc

if der==0
%     output = -log(sum(wtedphi,2));
    output = -llhfunc(x(1:mog.dim,:)');
    output = output + .5*x(end)^2;
elseif der==1
%     frac = wtedphi./repmat(sum(wtedphi,2),1,mog.k);
%     output = sum(repmat(frac,mog.dim,1).*cell2mat(cellfun(@(mu,Sigma)Sigma\(x(1:mog.dim,:)-mu'),mat2cell(mog.mu',ones(1,mog.k),mog.dim)',mat2cell(reshape(mog.cv,mog.dim,mog.dim*mog.k),mog.dim,mog.dim*ones(1,mog.k)),'UniformOutput', false)),2);
    output = -gradfunc(x(1:mog.dim,:))';
    if length(x)>mog.dim
        output = [output; x(end)];
    end
else
    disp('wrong choice of der!');
end

end


% potential energy after surgery %

function [f,g] = U_surg(x,T)
if(nargin<2)
    T=1.05;
end

global mog mode

global llhfunc gradfunc llhfunc_m gradfunc_m

[LLH,Ind]=sort([llhfunc(x(1:mog.dim,:)'),llhfunc_m(x(1:mode.dim,:)')]);

f = -(LLH(2)/T+log(1-exp(LLH(1)-LLH(2)/T)));

if nargout>1
    GRAD=[gradfunc(x(1:mog.dim,:))',gradfunc_m(x(1:mode.dim,:))'];
    GRAD=GRAD(:,Ind);
    g = -(GRAD(:,2)./T+1/(1-exp(LLH(1)-LLH(2)/T))*(-exp(LLH(1)-LLH(2)/T).*(GRAD(:,1)-GRAD(:,2)./T)));
end

end


% tweak measure to generate a tunnel between modes
function [G InvG dG Gamma1] = Met(x,opt)
if(nargin<2)
    opt=0;
end

global mog tunnel

M_base = eye(mog.dim+1);

% [~,closest] = min(sum((repmat(x(1:mog.dim),1,mog.k)-mog.mu).^2));
% if x(end)<0
%     tnl.L = [mog.mu(:,closest); -1]; tnl.R = [mog.mu; ones(1,mog.k)];
%     tnl.vec = cat(2,tunnel.upvec{closest,:});
%     tnl.met = cat(3,tunnel.upmet{closest,:});
% else
%     tnl.L = [mog.mu(:,closest); 1]; tnl.R = [mog.mu; -ones(1,mog.k)];
%     tnl.vec = cat(2,tunnel.dnvec{closest,:});
%     tnl.met = cat(3,tunnel.dnmet{closest,:});
% end
% X2tnlL = repmat(x-tnl.L,1,mog.k); X2tnlR = repmat(x,1,mog.k)-tnl.R;
% projL = sum(X2tnlL.*tnl.vec); projR = sum(X2tnlR.*tnl.vec);
% vicinity = sum(X2tnlL.*X2tnlR) + abs(projL.*projR);
% dvicinity = X2tnlL+X2tnlR + repmat(sign(projL).*abs(projR) + sign(projR).*abs(projL),mog.dim+1,1).*tnl.vec;
% 
% moll = exp(-vicinity/mog.dim/tunnel.influence); dmoll = repmat(-moll/mog.dim/tunnel.influence,mog.dim+1,1).*dvicinity;
% wtTM = zeros(mog.dim+1);
% for c=1:mog.k
%     wtTM = wtTM + moll(c).*tnl.met(:,:,c);
% end
% wtTM(1:mog.dim,end) = 0; wtTM(end,1:mog.dim) = 0;
% G = (1-sum(moll)).*M_base + wtTM;
G = M_base;

if all(opt==0)
    InvG=NaN; dG=NaN; Gamma1=NaN;
else
    if any(opt==-1)
        InvG = inv(G);
    end
    if any(opt==1)
        dM_base = zeros(repmat(mog.dim+1,1,3));
        dG = zeros(repmat(mog.dim+1,1,3));
%         for c=1:mog.k
%             dG = dG + reshape(kron(dmoll(:,c)',-M_base+tnl.met(:,:,c)),repmat(mog.dim+1,1,3));
%         end
%         dG = dG + (1- sum(moll)).*dM_base;
        if any(opt==3)
            Gamma1 = .5*(permute(dG,[1,3,2]) + permute(dG,[3,2,1]) - dG);
        else
            Gamma1=NaN;
        end
    else
        dG=NaN; Gamma1=NaN;
    end
end

end


