function [xNew,accept,err,P,jumped] = whmcfunc(xcurr,llhfunc,gradfunc,mode,tunnel,w_hmc)

dim = length(xcurr);
lfnum = w_hmc.lfnum;
lfsize = w_hmc.lfsize;
ntnum = w_hmc.ntnum;

xNew = xcurr;
% v = sM*randn(dim,1);
v = randn(dim,1);
Ex = -llhfunc(xNew(1:end-1)')+.5*xNew(end)^2;
Hx = Ex + 0.5*(v'*v);

% randomize leapfrog jumps
if 1
    lfnum_t = randi([lfnum-1 lfnum+1]);
    lfsize_t = lfsize+ (rand()-0.5)*lfsize/10;
end


jumped = 0; EnergyAdj = 0; adjusted = 0;
% leapfrogs
for i=1:lfnum_t

    dX = [gradfunc(xNew(1:end-1))';-xNew(end)];
    v = v + 0.5*lfsize_t*dX;

    if jumped
        xNew = xNew + lfsize_t*v;
    else
        xTemp = xNew;
        [~,closest] = min(sum((repmat(xTemp(1:mode.dim),1,mode.k)-mode.center).^2));
        tnl.length = tunnel.length(closest,:);
        if xTemp(end)<0
            tnl.L = [mode.center(:,closest); -1]; tnl.R = [mode.center; ones(1,mode.k)];
            tnl.vec = cat(2,tunnel.upvec{closest,:});
        else
            tnl.L = [mode.center(:,closest); 1]; tnl.R = [mode.center; -ones(1,mode.k)];
            tnl.vec = cat(2,tunnel.dnvec{closest,:});
        end
        for FixedPointIteration = 1:ntnum

            X2tnlL = repmat(xTemp-tnl.L,1,mode.k); X2tnlR = repmat(xTemp,1,mode.k)-tnl.R;
            projL = sum(X2tnlL.*tnl.vec); projR = sum(X2tnlR.*tnl.vec);
            vicinity = sum(X2tnlL.*X2tnlR) + abs(projL.*projR);
            moll = exp(-vicinity/mode.dim./tunnel.width(closest,:));
            if rand<1-sum(moll)
                Move = v;
            else
                jump2 = randsample(mode.k,1,true,moll);
                Move = -2/lfsize.*X2tnlR(:,jump2);
                jumped = 1;
            end
            if FixedPointIteration==1
                Movefixed = Move;
            end

            xTemp = xNew + .5*lfsize.*(Movefixed+Move);
        end
        [~,closestNew] = min(sum((repmat(xTemp(1:mode.dim),1,mode.k)-mode.center).^2));
        if closestNew~=closest
            EnergyAdj = (-llhfunc(xTemp(1:end-1)')+.5*xTemp(end)^2) - (-llhfunc(xNew(1:end-1)')+.5*xNew(end)^2+(v'*v)/2);
            adjusted = 1;
        end
        xNew = xTemp;
    end

    dX = [gradfunc(xNew(1:end-1))';-xNew(end)];      
    v = v + 0.5*lfsize_t*dX;
    if adjusted
        EnergyAdj = EnergyAdj + (v'*v)/2;
        adjusted = 0;
    end

end

Ey = -llhfunc(xNew(1:end-1)')+.5*xNew(end)^2;
Hy = Ey + 0.5*(v'*v);

% accept-reject
P = min(1,exp(Hx-Hy+EnergyAdj));
accept = rand < P;

%% Accept-Reject 
if accept == 1
    err = Ey;
else
    xNew = xcurr;
    err = Ex;   
end