function [] = energycontour( )

addpath('../../00_utils');
% Random Numbers...
randn('state', 2013);
rand('twister', 2013);
warning('OFF');

% data
% global mog
dim = 100; k = 10; % Dimension and Components
compdist = 20;
[mu,cv,invcv] = getmog_1(k,dim,compdist); 
mog.mu = mu + 1; 
mog.cv = cv; 
mog.invcv = invcv;
mog.k = k; 
mog.dim = dim;
pik = ones(1,k);
mog.pik = pik./sum(pik);

% known modes
% global mode
mode.center=mog.mu(:,1:2:end);
mode.hess=mog.cv(:,:,1:2:end);
[mode.dim, mode.k]=size(mode.center);
mode.wt=mog.pik(1:2:end);

% unknown modes
umode.center=mog.mu(:,2:2:end);
umode.hess=mog.cv(:,:,2:2:end);
[umode.dim, umode.k]=size(umode.center);
umode.wt=mog.pik(2:2:end);

% draw posterior
fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
set(0,'CurrentFigure',fig1);
linewidth = 1;

nfig = 1;
figrow = 1;
figcol = nfig/figrow;
% cvpair = [1:nfig; mod((1:nfig) + dim/2 -1, dim)+1]';
cvpair = [1 2; 3 4; 5 6; 7 8];
colors = distinguishable_colors(mog.k);

for f=1:nfig
    
    subplot(figrow,figcol,f);        
    hold on;
    ii = cvpair(f,1);
    jj = cvpair(f,2);

    for d=1:mog.k
        plotGauss(mog.mu(ii,d),mog.mu(jj,d),...
                 mog.cv(ii,ii,d),mog.cv(jj,jj,d),mog.cv(ii,jj,d),...
                 2,'--k',linewidth); hold on;  
%         plot(mog.mu(ii,kk),mog.mu(jj,kk),'color',colors(kk,:),'marker','x','markersize',10,'linewidth',2);                        
        plot(mog.mu(ii,d),mog.mu(jj,d),'k+','markersize',9,'linewidth',2);
%         text(mog.mu(ii,d)+.5,mog.mu(jj,d),num2str(d));
    end
    
    axis tight
    axax(f,:) = axis*1.2;

end
ax = max(abs(axax),[],1);
ax([1 3]) = -1*ax([1 3]);
rangx = linspace(ax(1),ax(2),100);
rangy = linspace(ax(3),ax(4),100);
[xx yy] = meshgrid(rangx,rangy);
xv = xx(:);
yv = yy(:);
xy = [xv yv];
prxy = zeros([size(xx)  mog.k]);

for i=1:nfig
    subplot(figrow,figcol,i); hold on;          
    ii = cvpair(i,1);
    jj = cvpair(i,2);
    density = exp(logmogpdf(xy,mode.center([ii jj],:),mode.hess([ii,jj],[ii,jj],:),mode.wt));              
    prxy(:,:,i) = reshape(density,length(rangy),length(rangx));    
    [c1,h1]=contour(xx, yy,prxy(:,:,i),15); %colormap(autumn);
end
set(h1,'color','r');

for i=1:nfig
    subplot(figrow,figcol,i); hold on;          
    ii = cvpair(i,1);
    jj = cvpair(i,2);
    density = exp(logmogpdf(xy,umode.center([ii jj],:),umode.hess([ii,jj],[ii,jj],:),umode.wt));              
    prxy(:,:,i) = reshape(density,length(rangy),length(rangx));    
    [c2,h2]=contour(xx, yy,prxy(:,:,i),15); %colormap(winter);
end
set(h2,'color','b');

xlim([-12,12]);ylim([-12,12]);
title('Energy Contour','FontSize',20);


% global llhfunc gradfunc llhfunc_m gradfunc_m
% get function handles
[llhfunc,gradfunc] = funcslogmog(mog);
% functions from known modes
[llhfunc_m,gradfunc_m] = funcslogmode(mode);

fig2=figure(2); clf;
% set(fig2,'windowstyle','docked');
set(0,'CurrentFigure',fig2);
T=1.05;

for i=1:nfig
    subplot(figrow,figcol,i); hold on;          
    ii = cvpair(i,1);
    jj = cvpair(i,2);
    densitydiff = -exp(logmogpdf(xy,mode.center([ii jj],:),mode.hess([ii,jj],[ii,jj],:),mode.wt)/T)+exp(logmogpdf(xy,mog.mu([ii jj],:),mog.cv([ii,jj],[ii,jj],:),mog.pik));              
    prxy(:,:,i) = reshape(densitydiff,length(rangy),length(rangx));    
    contour(xx, yy,prxy(:,:,i),15); colormap(winter);
end
title(['Residual Energy Contour (T=',num2str(T),')'],'FontSize',20);
text(5,4.4,'Known Modes','Fontsize',18,'color','r');
annotation('arrow',[.7,.63],[.65,.76],'color','r');
annotation('arrow',[.79,.79],[.62,.43],'color','r');
annotation('arrow',[.62,.25],[.64,.67],'color','r');
annotation('arrow',[.62,.38],[.65,.82],'color','r');
annotation('arrow',[.79,.6],[.62,.3],'color','r');
text(-8.5,-8.5,'Unknown Modes','Fontsize',18,'color','b');
annotation('arrow',[.33,.33],[.27,.42],'color','b');
annotation('arrow',[.33,.43],[.27,.5],'color','b');
annotation('arrow',[.33,.67],[.27,.49],'color','b');
annotation('arrow',[.33,.4],[.27,.77],'color','b');
annotation('arrow',[.49,.72],[.25,.24],'color','b');

end