% clear
% close all
% addpath('drawdata');
addpath('../../00_utils');
dbstop if error

% time plot for K=10,D=10,100

Dim = [10,100];
ndim = length(Dim);
colors = distinguishable_colors(ndim);
styles = {'-','--','-.',':','--x'};


files = dir('./result');
nfiles = length(files) - 2;

for d=1:ndim
    for i=1:nfiles
        if ~isempty(strfind(files(i+2).name,['d',num2str(Dim(d)),'_k10']))
            mc{d} = load(strcat('./result/', files(i+2).name));
        end
    end
end


% set common time points
for i=1:ndim
    ixt = sum(mc{i}.TIME&mc{i}.TIME < 800);
    cuttime(i) = ixt;
end
% cuttime =[floor(1:cuttime(1)/4:cuttime(1)) cuttime];

fig.mode = figure(200); clf;
% set(fig.mode,'windowstyle','docked');

%% Number of Modes
for i=1:ndim
    [XX,YY] = stairs(mc{i}.TIME(1:cuttime(i)), mc{i}.Nmodes(1:cuttime(i)));
    plot(XX,YY, styles{1+i},'color',colors(i,:),'linewidth',3); hold on;
end
drawnow;
ylim([1,11]);
set(gca,'FontSize',15);
xlabel('Seconds','FontSize',18); ylabel('Number of Modes Discovered','FontSize',18); 
legend('D=10','D=100','FontSize',18,'location','SouthEast');
% title('Discovering Modes','FontSize',20);
plot([0,800],repmat(10+.01,1,2),'-k','linewidth',1.5); hold on;

hold off;
