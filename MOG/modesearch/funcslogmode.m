function [llh1,grad1] = funcslogmode(mode)

    llh1 = @(w)llhmode(w,mode);
    grad1 = @(w)gradlogmode(w,mode);
    
    % influence radius
    r=1;


    %% log likelihood
    function llh1 = llhmode(X,mode)

        %x = x';
        mu = mode.center;
        cv = mode.hess;
        pik = mode.wt;

        [~, k] = size(mu);
        n = size(X,1);

        logpk = zeros(n,k);
        for i=1:k
            logpk(:,i) = logmvnpdf(X,mu(:,i)',cv(:,:,i));
        end

        logpk = logpk + repmat(log(pik),n,1);
        llh1 = logsumexp(logpk,2);

    end

    %% grad log mog
    function dlogmode = gradlogmode(w,mode)
        
        mu = mode.center; cv = mode.hess; pik = mode.wt;
        
        [dim,k] = size(mu);
        b = zeros(k,dim);
        logpkx = zeros(1,k);

        for kk=1:k
            mui = mu(:,kk);
            cvi = cv(:,:,kk);
            logpkx(kk) = logmvnpdf(w',mui',cvi);
            b(kk,:) = cvi\(mui-w);           
        end

        ret = 0;
        for kk=1:k
            ret = ret + pik(kk)*b(kk,:)/(exp(logpkx - repmat(logpkx(kk),1,k))*pik');
        end

        dlogmode = ret;

        if isnan(dlogmode)
            error('nan');
        end
        
    end
      

end