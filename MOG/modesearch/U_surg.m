function [f,g] = U_surg(x,llhfunc,gradfunc,mode,llhfunc_m,gradfunc_m,T)
if(nargin<7)
    T=1.05;
end


[LLH,Ind]=sort([llhfunc(x'),llhfunc_m(x')]);

f = -(LLH(2)/T+log(1-exp(LLH(1)-LLH(2)/T)));

if nargout>1
    GRAD=[gradfunc(x)',gradfunc_m(x)'];
    GRAD=GRAD(:,Ind);
    g = -(GRAD(:,2)./T+1/(1-exp(LLH(1)-LLH(2)/T))*(-exp(LLH(1)-LLH(2)/T).*(GRAD(:,1)-GRAD(:,2)./T)));
end

end