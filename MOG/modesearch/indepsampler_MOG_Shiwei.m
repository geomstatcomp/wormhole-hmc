function [wnew,ishopaccept,prhop,isregen,prregen,numreject] = ...
    indepsampler_MOG_Shiwei(wcurr,llhfunc,mode,maxfreqlogwx,type)

% type:
%       0=> method1 (no rejection)
%       1 => method2 proposal (reject+mh) ~ min{p(x), mf(x)} + MH 
%       2 => method2 after regen proposal ~ min{p(x), mf(x)}
%       3 => method1 after regen proposal ~ f(y) and accept by min{1,w(y)/C} 
%       4,5 => method3 proposal ~ uniform truncated ellipse

% global t

% if type == 4 || type == 5
%   assert(dpgm.uniform);
% end

ishopaccept = 0;
prhop = 0;
isregen = 0;
prregen = 0;
numreject = 0;

if type == 1
    logC = 1;
else
    logC = maxfreqlogwx; % - log(2);
end


% propose from gm
dosample = 1;    

while dosample

    numreject = numreject + 1;
%     ixk = find(mnrnd(1,dpgm.pik));    
    sum_pik = cumsum(mode.wt);
    ixk = sum(rand * sum_pik(end) > sum_pik) + 1;
    
    wprop = mvnrnd(mode.center(:,ixk),mode.hess(:,:,ixk));
    logpxprop = llhfunc(wprop);
    logfxprop = logmogpdf(wprop,mode.center,mode.hess,mode.wt);

    if type == 0 % method 1
        dosample = 0;
    elseif type == 1 || type == 2  %  ~ min{p(x), mf(x)}
        prrejectaccept = min(1,exp(logpxprop-logfxprop));
        dosample = prrejectaccept < 1 && rand > prrejectaccept;
        logfxprop = min(logpxprop, logfxprop);
    elseif type == 3  % ~ min{1,w(y)/C}
        prrejectaccept = min(1,exp(logpxprop-(logfxprop+logC)));
        dosample = rand > prrejectaccept;
    elseif type == 4 || type == 5
        dosample = 0;
    else
        error('undefined type');
    end
end    


if type == 2 || type == 3    
    wnew = wprop'; 
    return  % return without MH
else
    % MH Step for type 0 and 1
    % curr
    logpxcurr = llhfunc(wcurr');
    logfxcurr = logmogpdf(wcurr',mode.center,mode.hess,mode.wt);
    if type == 1
      logfxcurr = min(logpxcurr, logfxcurr);
    end

    % compute pr_accept
    logwx = logpxcurr - logfxcurr;
    logwy = logpxprop - logfxprop;
    prhop = min(1,exp(logwy-logwx));

    % test regeneration 
    ishopaccept = rand < prhop;
    prregen = 0;
    if ishopaccept
        wnew = wprop';
        prregen = min(exp(logC-logwx),1)*min(exp(logwy-logC),1) / prhop;
        % adapt if regenerated
        isregen = rand < prregen;
    else
        isregen = 0;
        wnew = wcurr;
    end
end