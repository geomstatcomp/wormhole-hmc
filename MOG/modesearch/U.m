function [f,g] = U(x,llhfunc,gradfunc)

f=-llhfunc(x');

if nargout>1
    g = -gradfunc(x)';
end

end