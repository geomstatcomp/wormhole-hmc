function run_RWHMC_P_mog_Search(seed,dim,...
                                     REJECPROP,maxncompo,...
                                     iterburnin,nrestart,SAMP4ADAPT,nsubsamp,...
                                     mxit,itvplot,itvprint,itvsave,PLOT,...
                                     mog,w_hmc,nparallel,maxruntime,domixchain,useRswitch)

% clear
% close all
dbstop if error
addpath('../../package/vdpgm');
addpath('../../00_drmcmc_core');
addpath('../../00_utils');


if isdeployed
        
        seed = str2double(seed);
        dim = str2double(dim);
        REJECPROP = str2double(REJECPROP);
        maxncompo = str2double(maxncompo);
        SAMP4ADAPT = str2double(SAMP4ADAPT); % minimum number of samples to add for dpmm update-- control freq of updates
        nsubsamp = str2double(nsubsamp); % number of sub-samples from the combined chain for dpmm update
        iterburnin = str2double(iterburnin); 
        mxit = str2double(mxit);
        itvplot = str2double(itvplot); % plotting interval
        itvsave = str2double(itvsave); % saving interval
        itvprint = str2double(itvprint); % printing interval
        PLOT = str2double(PLOT);
        nparallel = str2double(nparallel);
        maxruntime = str2double(maxruntime);
        
end

dock = 1;

%% random stream
% RandStream.setDefaultStream(RandStream('mt19937ar','Seed',seed));
RandStream.setGlobalStream(RandStream('mt19937ar','Seed',seed))


%% Init Parallelization
% nparallel = 2;
dpgmalgo = 1;   
% useRswitch = 1;
search_onthefly = 1;
samp4adapt_inc = 1.00;

sz.pool = matlabpool('size');

if sz.pool > 0 && sz.pool ~= nparallel && nparallel ~= 0
        
        matlabpool close
        matlabpool(nparallel);        
        
end

if sz.pool == 0 && nparallel ~= 0
        
        matlabpool(nparallel);
        
end

maxNumCompThreads(2);

%% get function handles
[llhfunc,gradfunc] = funcslogmog(mog);
% clear ld

%% fname
fname = sprintf('rwhmc_mog_para_RJ%d_sd%d_d%d_k%d_para%d_nrestart%d_mix%d_rt%d', REJECPROP,seed, dim, mog.k, nparallel,nrestart,domixchain,maxruntime);
fname = strrep(fname,'.','-');
fname = appendfilehead(fname);
fname = strcat('_',fname);

%% plots
nhist = 1;
for j = 1:nhist
        fig.hist3(j) = figure(540+j); clf;
end

fig.rerror_mu = figure(560); clf;
% fig.rerror_cov = figure(551); clf;
fig.R = figure(562); clf;
% fig.locerr = figure(553); clf;

if dock && PLOT && ~isdeployed
        
        for j = 1:nhist
                set(fig.hist3(j),'windowstyle','docked');
        end
        set(fig.rerror_mu,'windowstyle','docked');   
%         set(fig.rerror_cov,'windowstyle','docked');   
        set(fig.R,'windowstyle','docked');   
%         set(fig.locerr,'windowstyle','docked');
        
end

pause(0.5);
figrow = 1;
% nfig = figrow^2;
nfig = 1;
% nfig = 1;
nfig = min(nfig,dim-1);
% cvpair = [1:nfig; mod((1:nfig) + dim/2 -1, dim)+1]';
cvpair = [1 2; 3 4; 5 6; 7 8];
colors = distinguishable_colors(mog.k);

%% draw components
for qq = 1:nhist
        
        set(0,'CurrentFigure',fig.hist3(qq)); 
        linewidth = 1;

        for f=1:nfig

                subplot(figrow,figrow,f);        
                hold on;
                ii = cvpair(f,1);
                jj = cvpair(f,2);

                for kk=1:mog.k

                        plotGauss(mog.mu(ii,kk),mog.mu(jj,kk),...
                                         mog.cv(ii,ii,kk),mog.cv(jj,jj,kk),mog.cv(ii,jj,kk),...
                                         2,'--k',linewidth); hold on;  
%                         plot(mog.mu(ii,kk),mog.mu(jj,kk),'color',colors(kk,:),'marker','x','markersize',10,'linewidth',2);                        
                        plot(mog.mu(ii,kk),mog.mu(jj,kk),'k+','markersize',10,'linewidth',2);            


                end
                axis tight
                axax(f,:) = axis*1.2;

        end
        
end

drawnow;

%% draw mog hist3(qq)
ax = max(abs(axax),[],1);
ax([1 3]) = -1*ax([1 3]);
rangx = linspace(ax(1),ax(2),100);
rangy = linspace(ax(3),ax(4),100);
[xx yy] = meshgrid(rangx,rangy);
xv = xx(:);
yv = yy(:);
xy = [xv yv];
prxy = zeros([size(xx)  mog.k]);

for i=1:nfig

        subplot(figrow,figrow,i); hold on;          
        ii = cvpair(i,1);
        jj = cvpair(i,2);
        density = exp(logmogpdf(xy,mog.mu([ii jj],:),mog.cv([ii,jj],[ii,jj],:),mog.pik));              
        prxy(:,:,i) = reshape(density,length(rangy),length(rangx));    
        contour(xx, yy,prxy(:,:,i),15); colormap default
        
end

drawnow;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% preparing storage

% nburnin = iterburnin;
% SAMPBURNIN = zeros(dim, nrestart*nparallel);
% SAMP = zeros(dim, mxit, nparallel);
% TOUR = zeros(dim, ninitsamp, nparallel);
SAMP = cell(nparallel,1);
% TOUR = cell(nparallel,1);
for qq = 1 : nparallel
        SAMP{qq} = zeros(dim, mxit*2);
%         TOUR{qq} = zeros(dim, ninitsamp);
end

TIME = zeros(mxit,1);
avgprhmcaccept = zeros(nparallel,1);
prhmcaccept = zeros(nparallel,1);
s = zeros(nparallel,1);
discoveredmode = zeros(mog.k,1);

R = zeros(ceil(mxit/itvplot)+100,1);
CURR = zeros(ceil(mxit/itvplot)+100,1);
REMU = zeros(ceil(mxit/itvplot)+100,1);
REMUT = zeros(ceil(mxit/itvplot)+100,1);
HOPACCEPTRATE = zeros(ceil(mxit/itvplot)+100,nparallel);
REGENRATE = zeros(ceil(mxit/itvplot)+100,nparallel);

%% define modes
% global mode
mode.k=0; mode.dim=dim;
mode.center=[]; mode.hess=[]; mode.wt=[];

time_start = tic;
tottime = 0;
gt.mu = mog.pik*mog.mu';


options=optimoptions('fminunc','GradObj','on','MaxIter',100,'TolX',1e-8);
%% initial mode search
if nrestart > 0
        
        
        disp('start initial mode searching...');
        
        
        %% overdispersed init
        rangemin = min(mog.mu')*2;
        rangemax = max(mog.mu')*2;     
        
        stopmodesearch = 0;
        isalldiscovered = 0;
        
        
        while ~stopmodesearch

                % overdispersed initialization
                w = repmat(rangemin',1,nparallel) + rand(dim,nparallel).*repmat((rangemax'-rangemin'),1,nparallel);
                
                mode_cand=cell(1,nparallel); hessian=cell(1,nparallel); exitflag=cell(1,nparallel);
                parfor qq = 1:nparallel
                    
                        [mode_cand{qq},~,exitflag{qq},~,~,hessian{qq}]=fminunc(@(x)U(x,llhfunc,gradfunc),w(:,qq),options);
                end
                
                for qq = 1:nparallel
                        
                        % check discovered mode
                        if (exitflag{qq}==1||exitflag{qq}==3)&&all(eig(hessian{qq})>=1e-6)
                            mode.center=[mode.center,mode_cand{qq}];
                            mode.k=mode.k+1;
                            mode.hess(:,:,mode.k)=full(inv(hessian{qq}));
                            mode.hess(:,:,mode.k)=(mode.hess(:,:,mode.k)+mode.hess(:,:,mode.k)')./2; % for higher dimension, e.g. 100
                        end
                        
                        discoveredmode(max(abs(mog.mu-repmat(mode_cand{qq},1,mog.k)))<1e-2)=1;
                end
                
                sprintf('%d/%d modes discovered!',sum(discoveredmode),mog.k)
                                        
                
                %% stop search?
                
                if any(discoveredmode)
                        stopmodesearch = 1;
                        if all(discoveredmode)
                            isalldiscovered = 1;
                        end
                end
                
                %% plot
                % itv = 50;                       
                % if ~rem(t+itv,itv)
                if 1

                        for qq = 1:nhist

                                set(0,'CurrentFigure',fig.hist3(qq)); 

                                for ff = 1:nfig

                                        subplot(figrow,figrow,ff);             
                                        ii = cvpair(ff,1);
                                        jj = cvpair(ff,2);   

                                        contour(xx, yy,prxy(:,:,ff),10); hold on;

                                        for kk = 1:mog.k

                                                plot(mog.mu(ii,kk),mog.mu(jj,kk),'k+','markersize',10,'linewidth',2);            

                                        end

                                        plot(mode_cand{qq}(ii),mode_cand{qq}(jj),'rx','markersize',10,'linewidth',2);                
                                        axis(axax(ff,:)); 
                                        drawnow

                                end
                        end                                     
                end
                        
        end
        mode.wt = ones(1,mode.k)./mode.k; % initial weights
        
        disp('Initial mode searching finished!');
        
        
        %% combine tours
        CSAMP = [];
        s_init =1;
        s = s_init;
        
        time_presearch = toc(time_start);

        %% first dpgm update   
        ixsubsamp = cell(nparallel,1);      
        
%         save latestprerun_3d_5k.mat
%         save latestprerun_3d_5k_nomix.mat

else
        
        load latestprerun_3d_5k.mat
        time_start = tic;
                
end


for qq = 1:nhist

        set(0,'CurrentFigure',fig.hist3(qq)); 

        for ff = 1:nfig

                for kk = 1:mode.k

                        muk = mode.center(:,kk);
                        cvk = mode.hess(:,:,kk);

                        plotGauss(muk(ii), muk(jj),...
                                cvk(ii,ii),cvk(jj,jj),cvk(ii,jj),...
                                2,'--r',2);                                          
                end
                
        end
        
end

time4init = toc(time_start);
TIME(s) = time4init;
tottime = tottime + time4init;

%% get function handles with known modes
[llhfunc_m,gradfunc_m] = funcslogmode(mode);

%% define tunnel
% constructing super-tunnel:  network
tunnel.length = zeros(mode.k); tunnel.width = zeros(mode.k);
tunnel.upvec = cell(mode.k); tunnel.dnvec = cell(mode.k);
tunnel.upmet = cell(mode.k); tunnel.dnmet = cell(mode.k);
for i=1:mode.k
    [V,D]=eig(mode.hess(:,:,i));
    for j=i:mode.k
        tunnel.length(i,j) = norm([mode.center(:,j)-mode.center(:,i); 2]); % extra-dimension 2=1-(-1);
        upvec = [mode.center(:,j)-mode.center(:,i); 2]./tunnel.length(i,j); tunnel.upvec{i,j} = upvec;
        dnvec = upvec; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
        tunnel.width(i,j) = 2*D(end,end)*norm(V(:,end)-upvec(1:mode.dim).*(upvec(1:mode.dim)'*V(:,end)));
        tunnel.upmet{i,j} = eye(mode.dim+1)-(1-w_hmc.lfsize/20).*(upvec*upvec');
        tunnel.dnmet{i,j} = eye(mode.dim+1)-(1-w_hmc.lfsize/20).*(dnvec*dnvec');
    end
    for j=1:i-1
        tunnel.length(i,j) = tunnel.length(j,i);
        upvec = -tunnel.upvec{j,i}; upvec(end)=-upvec(end); tunnel.upvec{i,j} = upvec;
        dnvec = -tunnel.dnvec{j,i}; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
        tunnel.width(i,j) = 2*D(end,end)*norm(V(:,end)-upvec(1:mode.dim).*(upvec(1:mode.dim)'*V(:,end)));
        tunnel.upmet{i,j} = eye(mode.dim+1)-(1-w_hmc.lfsize/20).*(upvec*upvec');
        tunnel.dnmet{i,j} = eye(mode.dim+1)-(1-w_hmc.lfsize/20).*(dnvec*dnvec');
    end
end
tunnel.influence = .1;
tunnel.width = tunnel.influence.*tunnel.width;

 
%% compute R
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sampling 
% wcurr = SAMP(:,s,:);       
% wcurr = zeros(dim, nparallel);
wcurr = w;

t = 0; % iter
p = 0; % plot
v = 0; % save
% s = 0; % sample
r = 0; % MPSRF (Multivariate Potential Scale Reduction Factor)

% return from independent sampler
wgm = zeros(dim, nparallel);
whmc = zeros(dim+1, nparallel);
ishopaccept = zeros(1, nparallel);
prhop_t = zeros(1, nparallel);
isregen = zeros(1, nparallel);
prregen_t = zeros(1, nparallel);
nreject = zeros(1, nparallel);
nreject_t = zeros(1, nparallel);
nhopaccept = zeros(1, nparallel);
sumprregen = zeros(1, nparallel);
sumprhopaccept = zeros(1, nparallel);
njumptry = zeros(1, nparallel);
nadapt = zeros(1, nparallel);
nhmc = zeros(1, nparallel);
nhmcaccept = zeros(1, nparallel);
hmcaccept = zeros(1, nparallel);
jump = zeros(1, nparallel);
njump = zeros(1, nparallel);
nregen = zeros(1, nparallel);
iterlastregen = ones(1,nparallel)*s;
iterafteradapt = zeros(1,nparallel);
lastupdate = ones(1,nparallel)*s;
samp4adapt = ones(1, nparallel)*SAMP4ADAPT;

Regeneratime = [];

% NEWSAMP = zeros(dim,ninitsamp,nparallel);
% NEWSAMP = cell(nparallel,1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ts = zeros(nparallel, 1);
s = ones(nparallel, 1);  % s is a vector from now on
last_t_plot = 0;
doadapt = 1;

maxfreqlogwx = zeros(1,nparallel); %??
while min(ts) < mxit && tottime < maxruntime
    
        tic
        it_sync = max(samp4adapt);
        
        parfor qq = 1:nparallel
                
                
%                 %% mixchain update of DPMM
% 
%                 if doadapt && domixchain && isregen(qq) && ~dpgm(qq).uniform
% 
%                         fprintf('Start DPMM adapt! id:%d t:%d, SAMP4ADAPT:%d\n',qq,ts(qq),samp4adapt(qq))
%                         
%                         dpgm(qq) = dpgaussmixture_Yutian(CSAMP,nsubsamp,maxncompo,dpgmalgo,dpgm(qq).uniform);
%                         offset(qq) = getmodeoffset(llhfunc,dpgm(qq),tgradius);
%                         maxfreqlogwx(qq) = histwx(CSAMP(:,dpgm(qq).ixsub)',llhfunc,dpgm(qq),offset(qq),tgradius,2);
% 
%                         nadapt(qq) = nadapt(qq) + 1;
%                         lastupdate(qq) = s(qq);
%                         samp4adapt(qq) = floor(samp4adapt(qq)*samp4adapt_inc);
% 
%                         fprintf('DPMM adapted! id:%d t:%d, SAMP4ADAPT:%d\n',qq,ts(qq),samp4adapt(qq))
% 
%                         if REJECPROP  == 1% sample from  min{p(x), mf(x)}
% 
%                                 [wdpgm(:,qq),~,~,~,~,numreject_t] = indepsampler_dpgm_Yutian(...
%                                         wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),2,tgradius);         
% 
%                         elseif REJECPROP == 0 % sample from f(x) and accept by min{1,w(y)/C}
% 
%                                 [wdpgm(:,qq),~,~,~,~,numreject_t] = indepsampler_dpgm_Yutian(...
%                                         wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),3,tgradius);                                                                
% 
%                         elseif REJECPROP == 4 || REJECPROP == 5
%                                 % do nothing
%                         else
%                                 error(fprintf('undefined REJECPROP: %d', REJECPROP));
%                         end
% 
%                         wcurr(:,qq) = wdpgm(:,qq);
%                         SAMP{qq}(:,s(qq)) = wcurr(:,qq);
%                         isregen(qq) = 0;
% 
%                 end                
                
                
                
                %% start a session
                session_end = ts(qq) + it_sync;                
                
                while ts(qq) < session_end
                        
                        if ~rem(ts(qq)+100,100)
                                sprintf('Chain %d, t:%d',qq,ts(qq))
                        end
                        
                        ts(qq) = ts(qq) + 1;
                        s(qq) = s(qq) + 1;
                        
                        % 1st KERNEL, wormhole HMC            
                        [whmc(:,qq),hmcaccept(qq),~,prhmcaccept(qq),jump(qq)] = whmcfunc([wcurr(:,qq);-1], llhfunc, gradfunc, mode, tunnel, w_hmc);
                        % avgprhmcaccept(qq) = (1-1/t) * avgprhmcaccept(qq)+ 1/t * prhmcaccept(qq);

                        nhmc(qq) = nhmc(qq) + 1;        
                        nhmcaccept(qq) = nhmcaccept(qq) + hmcaccept(qq);
                        njump(qq) = njump(qq) + jump(qq);

                        red1dim=whmc(:,qq); red1dim(end)=[];
                        wcurr(:,qq) = red1dim;
   
                        % 2nd KERNEL, MOG independent sampler                     
                        if 1 %isintg(qq)                                    
                                
                                % propose from mixture of gaussian
                                [wgm(:,qq),ishopaccept(qq),prhop_t(qq),isregen(qq),prregen_t(qq),nreject_t(qq)] = ...
                                        indepsampler_MOG_Shiwei(...
                                                wcurr(:,qq),llhfunc,mode,maxfreqlogwx(qq),REJECPROP);            

                                % compute some statistics
                                nreject(qq) = nreject(qq) + nreject_t(qq);
                                nhopaccept(qq) = nhopaccept(qq) + ishopaccept(qq);
                                njumptry(qq) = njumptry(qq) + 1;
                                nregen(qq) = nregen(qq) + isregen(qq);
                                sumprregen(qq) = sumprregen(qq) + prregen_t(qq);
                                sumprhopaccept(qq) = sumprhopaccept(qq) + prhop_t(qq);                        

                                if ishopaccept(qq)                      
                                        
                                        wcurr(:,qq) = wgm(:,qq);                           
                                        
                                        if isregen(qq) && doadapt

                                                iterlastregen(qq) = s(qq);

                                                if samp4adapt(qq) < s(qq) - lastupdate(qq) %??

                                                        if domixchain

                                                                break;

                                                        else

%                                                                 if search_onthefly
%                                                                         
%                                                                         disp('[SEARCHING MODE...]');
%                                                                         winit = rangemin' + rand(dim,1).*(rangemax'-rangemin');                                                                        
%                                                                         NEWSAMP = modesearch_mog(winit,nburnin,ninitsamp,llhfunc,gradfunc,hmc,dpgm(qq));
% 
%                                                                         if ~isempty(NEWSAMP)
%                                                                                 SAMPX = [SAMP{qq}(:,1:s(qq)) NEWSAMP];
%                                                                                 disp('[MODE!! DISCOVERED]');
%                                                                         else                        
%                                                                                 SAMPX = SAMP{qq}(:,1:s(qq));
%                                                                                 disp('[Not Found]');
%                                                                         end
% 
% %                                                                         SAMPX = SAMP{qq}(:,1:s(qq));        
%                                                                         
%                                                                 else
%                                                                         SAMPX = SAMP{qq}(:,1:s(qq));                                                                        
%                                                                 end
%                                                                 
%                                                                 fprintf('Start DPMM adapt! id:%d t:%d, SAMP4ADAPT:%d\n',qq,ts(qq),samp4adapt(qq))
%                                                                 
%                                                                 dpgm(qq) = dpgaussmixture_Yutian(SAMPX,nsubsamp,maxncompo,dpgmalgo,dpgm(qq).uniform);
%                                                                 offset(qq) = getmodeoffset(llhfunc,dpgm(qq),tgradius);
%                                                                 maxfreqlogwx(qq) = histwx(SAMPX',llhfunc,dpgm(qq),offset(qq),tgradius,2);
% 
%                                                                 nadapt(qq) = nadapt(qq) + 1;
%                                                                 lastupdate(qq) = s(qq);
%                                                                 samp4adapt(qq) = floor(samp4adapt(qq)*1.05);
% 
%                                                                 fprintf('DPMM adapted! id:%d t:%d, SAMP4ADAPT:%d\n',qq,ts(qq),samp4adapt(qq))
% 
%                                                                 if REJECPROP  == 1% sample from  min{p(x), mf(x)}
% 
%                                                                         [wgm(:,qq),~,~,~,~,numreject_t] = indepsampler_dpgm_Yutian(...
%                                                                                 wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),2,tgradius);         
% 
%                                                                 elseif REJECPROP == 0 % sample from f(x) and accept by min{1,w(y)/C}
% 
%                                                                         [wgm(:,qq),~,~,~,~,numreject_t] = indepsampler_dpgm_Yutian(...
%                                                                                 wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),3,tgradius);                                                                
% 
%                                                                 elseif REJECPROP == 4 || REJECPROP == 5
%                                                                         % do nothing
%                                                                 else
%                                                                         error(fprintf('undefined REJECPROP: %d', REJECPROP));
%                                                                 end
% 
%                                                                 wcurr(:,qq) = wgm(:,qq);                                                                

                                                        end

                                                end
                 
                                        end
                                        
                                end

                        end
                        
                        SAMP{qq}(:,s(qq)) = wcurr(:,qq);
                        isregen(qq) = 0;                        
                        
                end
                
%                 disp('a')
                
        end
        
                
        if doadapt && domixchain
                
                % adapt modes (search new ones)%
                winit = repmat(rangemin',1,nparallel) + rand(dim,nparallel).*repmat((rangemax'-rangemin'),1,nparallel);
%                 winit = wcurr + 10*randn(dim,nparallel);%+ rand(dim,nparallel).*repmat((rangemax'-rangemin')/2,1,nparallel);
                
                parfor qq = find(isregen)
                                
                        sprintf('Chain %d,[SEARCHING MODE...]',qq)
                        
                        [mode_cand{qq},~,exitflag{qq},~,~,hessian{qq}]=fminunc(@(x)U_surg(x,llhfunc,gradfunc,mode,llhfunc_m,gradfunc_m),winit(:,qq),options);
                
                end
                
                for qq = find(isregen)
                        
                        % check discovered mode
                        if (exitflag{qq}==1||exitflag{qq}==3)&&all(eig(hessian{qq})>=1e-6)&&all(max(abs(mode.center-repmat(mode_cand{qq},1,mode.k)))>1e-2)
                            mode.center=[mode.center,mode_cand{qq}];
                            mode.k=mode.k+1;
                            mode.hess(:,:,mode.k)=inv(hessian{qq});
                            mode.hess(:,:,mode.k)=(mode.hess(:,:,mode.k)+mode.hess(:,:,mode.k)')./2; % for higher dimension, e.g. 100
                            sprintf('Chain %d, [Yay! DISCOVERED]',qq)
                        else                                                        
                            sprintf('Chain %d, [T..T NOT FOUND]',qq)
                        end
                        
                end
                
                %%%% mix chains %%%%
                for qq = find(isregen)
                        
                        ixnewtour = (lastupdate(qq)+1) : s(qq);
                        
                        CSAMP = [CSAMP SAMP{qq}(:,ixnewtour)];

                end
                
                lencombsamp = size(CSAMP,2);
                
                %%%% adapt indepsampler_MOG (weights) by dpgm %%%%
%                 if any(isregen)
%                     dpgm = dpgaussmixture_Yutian(CSAMP,nsubsamp,mode.k,dpgmalgo);
%                 end
%                 mode.wt=dpgm.pik; % update weights
                mode.wt=ones(1,mode.k)./mode.k;
                
                [llhfunc_m,gradfunc_m] = funcslogmode(mode); % update the function with newly found modes
                
                % reconstruct wormhole network %
                tunnel.length = zeros(mode.k); tunnel.width = zeros(mode.k);
                tunnel.upvec = cell(mode.k); tunnel.dnvec = cell(mode.k);
                tunnel.upmet = cell(mode.k); tunnel.dnmet = cell(mode.k);
                for i=1:mode.k
                    [V,D]=eig(mode.hess(:,:,i));
                    for j=i:mode.k
                        tunnel.length(i,j) = norm([mode.center(:,j)-mode.center(:,i); 2]); % extra-dimension 2=1-(-1);
                        upvec = [mode.center(:,j)-mode.center(:,i); 2]./tunnel.length(i,j); tunnel.upvec{i,j} = upvec;
                        dnvec = upvec; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
                        tunnel.width(i,j) = 2*D(end,end)*norm(V(:,end)-upvec(1:mode.dim).*(upvec(1:mode.dim)'*V(:,end)));
                        tunnel.upmet{i,j} = eye(mode.dim+1)-(1-w_hmc.lfsize/20).*(upvec*upvec');
                        tunnel.dnmet{i,j} = eye(mode.dim+1)-(1-w_hmc.lfsize/20).*(dnvec*dnvec');
                    end
                    for j=1:i-1
                        tunnel.length(i,j) = tunnel.length(j,i);
                        upvec = -tunnel.upvec{j,i}; upvec(end)=-upvec(end); tunnel.upvec{i,j} = upvec;
                        dnvec = -tunnel.dnvec{j,i}; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
                        tunnel.width(i,j) = 2*D(end,end)*norm(V(:,end)-upvec(1:mode.dim).*(upvec(1:mode.dim)'*V(:,end)));
                        tunnel.upmet{i,j} = eye(mode.dim+1)-(1-w_hmc.lfsize/20).*(upvec*upvec');
                        tunnel.dnmet{i,j} = eye(mode.dim+1)-(1-w_hmc.lfsize/20).*(dnvec*dnvec');
                    end
                end
                tunnel.width = tunnel.influence.*tunnel.width;
                
                % record adaptation time
                for qq = find(isregen)
                        nadapt(qq) = nadapt(qq) + 1;
                        lastupdate(qq) = s(qq);
                end
                
        end
        
        tottime = tottime + toc;
        %% save regeneration time %%
        if any(isregen)
            Regeneratime = [Regeneratime,max(iterlastregen)];
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% save
        if (floor(last_t_plot/itvsave) ~= floor(min(ts) / itvsave)) && p > 0
                
                v = v + 1;
                
                cd 00_log_working

                % PNG
%                 saveas(fig.hist3(1),strcat(fname,'_HIST1.png'),'png');
%                 saveas(fig.hist3(2),strcat(fname,'_HIST2.png'),'png');
%                 saveas(fig.R,strcat(fname,'_R.png'),'png');
%                 saveas(fig.rerror_mu,strcat(fname,'_REMU.png'),'png');
%                 saveas(fig.rerror_cov,strcat(fname,'_RECV.png'),'png');
%                 
%                 % FIG
%                 saveas(fig.hist3(1),strcat(fname,'_HIST1.fig'),'fig');
%                 saveas(fig.hist3(2),strcat(fname,'_HIST2.fig'),'fig');
%                 saveas(fig.R,strcat(fname,'_R.fig'),'fig');
%                 saveas(fig.rerror_mu,strcat(fname,'_REMU.fig'),'fig');
%                 saveas(fig.rerror_cov,strcat(fname,'_RECV.fig'),'fig');

                save(fname);
                % save(fname,'SAMP','s','seed','dim','lfn','lfsize','nrestart','ninitsamp','nburnin','nsubsamp');          
                
                disp('---------------- SAVE COMPLETE! ----------------');           
                cd ..                
                
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% print
%         if (floor(last_t_plot/itvprint) ~= floor(min(ts) / itvprint))
%                                 
%         end


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% plot    
        if (floor(last_t_plot/itvplot) ~= floor(min(ts) / itvplot))
                
                tic
                
                p = p + 1;
                        
                ts
                whmcacptrate = nhmcaccept./nhmc
                whmcjumprate = njump./nhmc
                hopacceptrate = nhopaccept./nhmc
                regenrate = nregen./nhmc
                HOPACCEPTRATE(p,:) = hopacceptrate;
                REGENRATE(p,:) = regenrate;
                
                
                %% R convergence Diagnostic                      
                SAMP_FOR_R = zeros(min(s)-s_init, dim, nparallel);
                for qq = 1 : nparallel
                        SAMP_FOR_R(:,:,qq) = SAMP{qq}(:,s_init+1:min(s))';
                end
                R(p) = mpsrf(SAMP_FOR_R);
                
             
                        
                nsamp4curR = 200;

                SAMP_FOR_CURR = zeros(nsamp4curR, dim, nparallel);

                for qq = 1 : nparallel
                        SAMP_FOR_CURR(:,:,qq) = SAMP{qq}(:,min(s)-nsamp4curR+1:min(s))';
                end

                CURR(p) = mpsrf(SAMP_FOR_CURR);

                if useRswitch
                        
                        if CURR(p) < 1.1 

                                doadapt = 0;
                                sprintf('%dt REGEN SWITCH [OFF]: curR=%d',t,CURR(p))
                                
                        else
                                
                                doadapt = 1;
                                sprintf('%dt REGEN SWITCH [ON]: curR=%d',t,CURR(p))
                                
                        end
                        
                end
                
                %% compute error
                combsamp = [];
                for qq=1:nparallel                        

                        sampmu = mean(SAMP{qq}(:,s_init:s(qq))');
                        REMU(p,qq) = sum(abs(gt.mu - sampmu))/sum(abs(gt.mu));
                        combsamp = [combsamp SAMP{qq}(:,s_init:s(qq))];
                        
                end                
                combsampmu = mean(combsamp,2);
                REMUT(p) = sum(abs(gt.mu - combsampmu'))/sum(abs(gt.mu));
                
                TIME(p) = tottime + toc;

                
                %%
                set(0, 'currentfigure', fig.R); clf        
                plot(TIME(1:p), R(1:p), '-+k'); hold on; 
%                 plot(TIME(1:p), CURR(1:p), '-ob'); hold on; 
                
                set(0, 'currentfigure', fig.rerror_mu); clf;                                
                if p > 1 && nparallel > 1
                        % plot(cumtime(itv), mean(REMU(1:p,:)'), '-r'); hold on;
%                         shadedErrorBar(TIME(1:p),mean(REMU(1:p,:)'), std(REMU(1:p,:)'),...
%                                 {'r-+','markerfacecolor','r'},1); hold on;
                        plot(TIME(1:p),REMUT(1:p),'-+b');
                else
%                         plot(TIME(1:p), mean(REMU(1:p,:)'),'-or'); hold on;
                        plot(TIME(1:p),REMUT(1:p),'-+b');
                end
                
                drawnow;
                               
                %% plot
                        
                for qq=1:nhist

                        set(0,'CurrentFigure',fig.hist3(qq)); 

                        for ff = 1:nfig

                                subplot(figrow,figrow,ff);             
                                ii = cvpair(ff,1);
                                jj = cvpair(ff,2);   

                                contour(xx, yy,prxy(:,:,ff),10); hold on;

                                for kk = 1:mog.k

%                                         plotGauss(mog.mu(ii,kk),mog.mu(jj,kk),...
%                                                          mog.cv(ii,ii,kk),mog.cv(jj,jj,kk),mog.cv(ii,jj,kk),...
%                                                          2,'--k',linewidth); hold on;
%                                         plot(mog.mu(ii,kk),mog.mu(jj,kk),'color',colors(kk,:),...
%                                                                         'marker','x','markersize',10,'linewidth',2);       
                                        plot(mog.mu(ii,kk),mog.mu(jj,kk),'k+','markersize',10,'linewidth',2);            

                                end
                                
                                for kk = 1:mode.k

                                        muk = mode.center(:,kk);
                                        cvk = mode.hess(:,:,kk);

                                        plotGauss(muk(ii), muk(jj),...
                                                cvk(ii,ii),cvk(jj,jj),cvk(ii,jj),...
                                                2,'--r',2);  
                                        
%                                         plotGauss(muk(ii), muk(jj),...
%                                                 cvk(ii,ii),cvk(jj,jj),cvk(ii,jj),...
%                                                 tgradius,'--m',2);  
                                        
                                end
                                
                                plot(SAMP{qq}(ii,s_init:s(qq)),SAMP{qq}(jj,s_init:s(qq)),'b.','markersize',1);
                                plot(wcurr(ii,qq),wcurr(jj,qq),'b.','markersize',10,'linewidth',2);                
                                if isregen(qq)
                                        plot(wcurr(ii,qq),wcurr(jj,qq),'ko','markersize',10,'linewidth',2);                
                                end                                
                                
                        end

                        hold off
                        axis(axax(ff,:));
                
                        title(strrep(fname,'_','-'));
                        drawnow;
                        
                end
        end
        
        last_t_plot = min(ts);
        
        
end

%% save when finished
fname = fname(2:end);
cd 00_log_finished

% saveas(fig.hist3(1),strcat(fname,'_HIST1.png'),'png');
% saveas(fig.hist3(2),strcat(fname,'_HIST2.png'),'png');
saveas(fig.R,strcat(fname,'_R.png'),'png');
saveas(fig.rerror_mu,strcat(fname,'_REMU.png'),'png');
% saveas(fig.rerror_cov,strcat(fname,'_RECV.png'),'png');
% 
% saveas(fig.hist3(1),strcat(fname,'_HIST1.fig'),'fig');
% saveas(fig.hist3(2),strcat(fname,'_HIST2.fig'),'fig');
% saveas(fig.R,strcat(fname,'_R.fig'),'fig');
% saveas(fig.rerror_mu,strcat(fname,'_REMU.fig'),'fig');
% saveas(fig.rerror_cov,strcat(fname,'_RECV.fig'),'fig');
for qq = 1:nparallel
        SAMP{qq} = SAMP{qq}(:,1:s(qq)); 
end
TIME = TIME(1:p);
REMU = REMU(1:p,:);
REMUT = REMUT(1:p);
R = R(1:p);
CURR = CURR(1:p);
save(fname);
cd ..
disp('---------------- RUN COMPLETE! ----------------');           
% save(fname,'SAMP','s','seed','dim','lfnum','lfsize','nrestart','ninitsamp','iterburnin','nsubsamp');
                 