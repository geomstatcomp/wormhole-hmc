%% iteration
clear

seed = 2013; 
mxit = 20000; 
itvplot = 20; itvsave = 20000; itvprint = 100; PLOT = 1;

%% drmcmc
m = 1; 
lfnum = 3; lfsize = 0.8; 
iterburnin = 1; 
troff = 5;

%% lmcwh
TrajectoryLength = 4;
NumOfLeapFrogSteps = 10;
StepSize = TrajectoryLength/NumOfLeapFrogSteps;


ninitsamp = 100; nsubsamp = 1000; samp4adapt = 200;

RUNTIME_K = 520;
RUNTIME_D = 820;


w_hmc.lfnum =10;
% w_hmc.lfsize = .4;
w_hmc.ntnum = 5;

%% MOG
compdist = 20;  
dim = 20; 

% maxruntime = [2000 2000 2000 2000];

hmc.lfnum = 10;
if dim == 10
        hmc.lfsize = 0.7; % dim = 10
elseif dim == 20
        hmc.lfsize = 0.5; % dim = 20
elseif dim == 40
        hmc.lfsize = .2; % dim = 40
elseif dim == 100
        hmc.lfsize = .1; % dim = 100
else
        hmc.lfsize = 1-(dim/50); % dim = 20
end
M = eye(dim);
hmc.iM = M;
hmc.sM = sqrtm(inv(M));

K_TEST = 0;
D_TEST = 1;

%%  K %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if K_TEST

        K = [5 10 15 20];
        % K = 10;
        maxruntime = ones(1,length(K))*RUNTIME_K;
        

        for j=1:length(K)

                k = K(j);
                runtime = maxruntime(j);
                if k<=10
                    mxit = 35000;
                    if k==10
                        runtime = 1020;
                    end
                end

                % DRMC
                nrestart = 100;

                maxncompo = k + 10;
%                 [mu,cv,invcv] = getmog(k,seed,dim,compdist); 
%                 mog.mu = mu + 1; 
%                 mog.cv = cv; 
%                 mog.invcv = invcv;
%                 mog.k = k; 
%                 mog.dim = dim;
%                 pik = ones(1,k);
%                 mog.pik = pik./sum(pik);
%                 save(['./model/mog_D' num2str(dim) '_K' num2str(k) '_seed' num2str(seed) '.mat'],'mog');
                load(['../model/mog_D' num2str(dim) '_K' num2str(k) '_seed2013.mat']);
                
                % tunnel
                tunnel.modes = mog.mu;
                tunnel.length = zeros(mog.k); tunnel.width = zeros(mog.k);
                tunnel.upvec = cell(mog.k); tunnel.dnvec = cell(mog.k);
                tunnel.upmet = cell(mog.k); tunnel.dnmet = cell(mog.k);
                for i=1:mog.k
                    [V,D]=eig(mog.cv(:,:,i));
                    for j=i:mog.k
                        tunnel.length(i,j) = norm([mog.mu(:,j)-mog.mu(:,i); 2]); % extra-dimension 2=1-(-1);
                        upvec = [mog.mu(:,j)-mog.mu(:,i); 2]./tunnel.length(i,j); tunnel.upvec{i,j} = upvec;
                        dnvec = upvec; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
                        tunnel.width(i,j) = 2*D(end,end)*norm(V(:,end)-upvec(1:mog.dim).*(upvec(1:mog.dim)'*V(:,end)));
                        tunnel.upmet{i,j} = eye(mog.dim+1)-(1-StepSize/20).*(upvec*upvec');
                        tunnel.dnmet{i,j} = eye(mog.dim+1)-(1-StepSize/20).*(dnvec*dnvec');
                    end
                    for j=1:i-1
                        tunnel.length(i,j) = tunnel.length(j,i);
                        upvec = -tunnel.upvec{j,i}; upvec(end)=-upvec(end); tunnel.upvec{i,j} = upvec;
                        dnvec = -tunnel.dnvec{j,i}; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
                        tunnel.width(i,j) = 2*D(end,end)*norm(V(:,end)-upvec(1:mog.dim).*(upvec(1:mog.dim)'*V(:,end)));
                        tunnel.upmet{i,j} = eye(mog.dim+1)-(1-StepSize/20).*(upvec*upvec');
                        tunnel.dnmet{i,j} = eye(mog.dim+1)-(1-StepSize/20).*(dnvec*dnvec');
                    end
                end
                tunnel.influence = .03;
                tunnel.width = tunnel.influence.*tunnel.width;

                DRMC_PC = 1;
                DRMC_P = 2;        
                DEMC = 3;
                LMCWH = 4;
                DMC_PL = 5;
                HMC_P = 6;
                DRMC_PC_RSWITCH = 7;
                DRMC_PC_SEARCH = 8;

        %         ALGO = 4;
                for ALGO = [2 4]

                        switch ALGO

                                case DRMC_PC_SEARCH

                                        disp('\nSTART DRMC_PC_SEARCH\n')

                                        nparallel = 1;      
                                        REJECPROP = 0;
                                        domixchain = 1;
                                        doinitmixchain = 1;
                                        domodesearch = 1;
                                        useRswitch = 0;  
                                        nrestart = 1;

                                        run_DRMC_P_mog_Search(seed,dim,troff,...
                                                                     REJECPROP,m,maxncompo,...
                                                                     ninitsamp,iterburnin,nrestart,samp4adapt,nsubsamp,...
                                                                     mxit,lfnum,lfsize,itvplot,itvprint,itvsave,PLOT,...
                                                                     mog,hmc,nparallel,runtime,domixchain,doinitmixchain,...
                                                                     useRswitch);   
                                                             
%                                         nparallel = 2;      
%                                                              
%                                         run_DRMC_P_mog_Search(seed,dim,troff,...
%                                                                      REJECPROP,m,maxncompo,...
%                                                                      ninitsamp,iterburnin,nrestart,samp4adapt,nsubsamp,...
%                                                                      mxit,lfnum,lfsize,itvplot,itvprint,itvsave,PLOT,...
%                                                                      mog,hmc,nparallel,runtime,domixchain,doinitmixchain,...
%                                                                      useRswitch);                                                                

                                        disp('\nEND DRMC_PC_SEARCH\n')
                                
                                case DRMC_PC

                                        disp('\nSTART DRMC_PC\n')

                                        nparallel = 4;      
                                        REJECPROP = 0;
                                        domixchain = 1;
                                        doinitmixchain = 1;
                                        domodesearch = 1;
                                        useRswitch = 0;
                                        
                                        nrestart = 1;


                                        run_DRMC_P_mog(seed,dim,troff,...
                                                                     REJECPROP,m,maxncompo,...
                                                                     ninitsamp,iterburnin,nrestart,samp4adapt,nsubsamp,...
                                                                     mxit,lfnum,lfsize,itvplot,itvprint,itvsave,PLOT,...
                                                                     mog,hmc,nparallel,runtime,domixchain,doinitmixchain,...
                                                                     useRswitch);   

                                        disp('\nEND DRMC_PC\n')

                                case DRMC_PC_RSWITCH

                                        disp('\nSTART DRMC_PC_RSWITCH\n')

                                        nparallel = 4;      
                                        REJECPROP = 0;
                                        domixchain = 1;
                                        doinitmixchain = 1;
                                        domodesearch = 1;
                                        useRswitch = 1;


                                        run_DRMC_P_mog(seed,dim,troff,...
                                                                     REJECPROP,m,maxncompo,...
                                                                     ninitsamp,iterburnin,nrestart,samp4adapt,nsubsamp,...
                                                                     mxit,lfnum,lfsize,itvplot,itvprint,itvsave,PLOT,...
                                                                     mog,hmc,nparallel,runtime,domixchain,doinitmixchain,...
                                                                     useRswitch);   

                                        disp('\nEND DRMC_PC_RSWITCH\n')

                                case DRMC_P 

                                        disp('\nSTART DRMC_P\n')

                                        nparallel = 1;     
                                        REJECPROP = 0;
                                        domixchain = 0;
                                        doinitmixchain = 1;
                                        domodesearch = 0;
                                        useRswitch = 0;

                                        run_DRMC_P_mog(seed,dim,troff,...
                                                                     REJECPROP,m,maxncompo,...
                                                                     ninitsamp,iterburnin,nrestart,samp4adapt,nsubsamp,...
                                                                     mxit,lfnum,lfsize,itvplot,itvprint,itvsave,PLOT,...
                                                                     mog,hmc,nparallel,runtime,domixchain,doinitmixchain,...
                                                                     domodesearch,useRswitch);

                                        disp('\nEND DRMC_P\n')


                                case DEMC

                                        disp('\nSTART DEMC\n')

                                        nparallel = 1;     
                                        stepsz = 1;
                                        npopu =20 + 10*k;
                                        noise = 0.0001;

                                        run_DEMC_mog(seed,dim,iterburnin,...
                                                                             mxit,itvplot,itvprint,itvsave,PLOT,mog,nparallel,stepsz,noise,npopu,runtime);

                                        disp('\nEND DEMC\n')


                                case LMCWH

                                        disp('\nSTART LMCWH\n')

                                        nparallel = 1;
                                        NewtonSteps = 6;

                                        run_LMCWH_mog_13(seed,dim,iterburnin,...
                                                                             mxit,itvplot,itvprint,itvsave,PLOT,mog,tunnel,...
                                                                             nparallel,TrajectoryLength,NumOfLeapFrogSteps,NewtonSteps,runtime);

                                        disp('\nEND LMCWT\n')

                                case DMC_PL

                                        disp('\nSTART DMC_PL\n')

                                        nparallel = 4;     
                                        REJECPROP = 5;
                                        domixchain = 0;
                                        doinitmixchain = 1;
                                        domodesearch = 1;
                                        useRswitch = 0;

                                        run_DRMC_P_mog(seed,dim,troff,...
                                                                     REJECPROP,m,maxncompo,...
                                                                     ninitsamp,iterburnin,nrestart,samp4adapt,nsubsamp,...
                                                                     mxit,lfnum,lfsize,itvplot,itvprint,itvsave,PLOT,...
                                                                     mog,hmc,nparallel,runtime,domixchain,doinitmixchain,...
                                                                     useRswitch);   

                                        disp('\nEND DMC_PL\n')


                                case HMC_P


                                        disp('\nSTART HMC_P\n')
                                        itersync = samp4adapt;
                                        iterburnin = 100;
                                        run_HMC_P_mog(seed,dim,iterburnin,...
                                                                     mxit,itvplot,itvprint,itvsave,PLOT,...
                                                                     mog,hmc,nparallel,runtime,itersync)

                                        disp('\nEND HMC_P\n')

                                otherwise

                                        error('undefined algorithm');

                        end

                end

        end

end


%%
if D_TEST

        D = [100];
        k = 10;
        maxruntime = ones(1,length(D))*RUNTIME_D;
        nrestart = 100;


        for j=1:length(D)

                dim = D(j);
                runtime = maxruntime(j);

                hmc.lfnum = 10;
                
                if dim == 5
                        hmc.lfsize = 0.8; % dim = 10                
                elseif dim == 10
                        hmc.lfsize = 0.7; % dim = 20
                        w_hmc.lfsize = .4;
                elseif dim == 15
                        hmc.lfsize = 0.6; % dim = 20
                elseif dim == 20
                        hmc.lfsize = 0.5; % dim = 20
                elseif dim == 40
                        hmc.lfsize = .2; % dim = 40
                elseif dim == 100
                        hmc.lfsize = .1; % dim = 100
                        w_hmc.lfsize = .1;
                else
                        
                end
                
%                 hmc.lfsize = 0.7; % dim = 20
                

                M = eye(dim);
                hmc.iM = M;
                hmc.sM = sqrtm(inv(M)); 
                
                % DRMC
                nrestart = 100;

                maxncompo = k + 10;
%                 [mu,cv,invcv] = getmog(k,seed,dim,compdist); 
%                 mog.mu = mu + 1; 
%                 mog.cv = cv; 
%                 mog.invcv = invcv;
%                 mog.k = k; 
%                 mog.dim = dim;
%                 pik = ones(1,k);
%                 mog.pik = pik./sum(pik);
%                 save(['./model/mog_D' num2str(dim) '_K' num2str(k) '_seed' num2str(seed) '.mat'],'mog');
%                 if dim<=10
                    load(['../model/mog_D' num2str(dim) '_K' num2str(k) '_seed2013.mat']);
%                 else
%                     load(['../model/mog_D' num2str(dim) '_K' num2str(k) '_seed2014.mat']);
%                 end
                
                % tunnel
                tunnel.modes = mog.mu;
                tunnel.length = zeros(mog.k); tunnel.width = zeros(mog.k);
                tunnel.upvec = cell(mog.k); tunnel.dnvec = cell(mog.k);
                tunnel.upmet = cell(mog.k); tunnel.dnmet = cell(mog.k);
                for i=1:mog.k
                    [V,Di]=eig(mog.cv(:,:,i));
                    for j=i:mog.k
                        tunnel.length(i,j) = norm([mog.mu(:,j)-mog.mu(:,i); 2]); % extra-dimension 2=1-(-1);
                        upvec = [mog.mu(:,j)-mog.mu(:,i); 2]./tunnel.length(i,j); tunnel.upvec{i,j} = upvec;
                        dnvec = upvec; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
                        tunnel.width(i,j) = 2*Di(end,end)*norm(V(:,end)-upvec(1:mog.dim).*(upvec(1:mog.dim)'*V(:,end)));
                        tunnel.upmet{i,j} = eye(mog.dim+1)-(1-StepSize/20).*(upvec*upvec');
                        tunnel.dnmet{i,j} = eye(mog.dim+1)-(1-StepSize/20).*(dnvec*dnvec');
                    end
                    for j=1:i-1
                        tunnel.length(i,j) = tunnel.length(j,i);
                        upvec = -tunnel.upvec{j,i}; upvec(end)=-upvec(end); tunnel.upvec{i,j} = upvec;
                        dnvec = -tunnel.dnvec{j,i}; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
                        tunnel.width(i,j) = 2*Di(end,end)*norm(V(:,end)-upvec(1:mog.dim).*(upvec(1:mog.dim)'*V(:,end)));
                        tunnel.upmet{i,j} = eye(mog.dim+1)-(1-StepSize/20).*(upvec*upvec');
                        tunnel.dnmet{i,j} = eye(mog.dim+1)-(1-StepSize/20).*(dnvec*dnvec');
                    end
                end
                tunnel.influence = .04;
                tunnel.width = tunnel.influence.*tunnel.width;

                DRMC_PC = 1;
                DRMC_P = 2;        
                DEMC = 3;
                LMCWH = 4;
                DMC_PL = 5;
                HMC_P = 6;
                DRMC_PC_RSWITCH = 7;
                DRMC_PC_SEARCH = 8;
                RWHMC_PC_SEARCH = 9;


                for ALGO = [9]

                        switch ALGO
                                
                                case DRMC_PC_SEARCH

                                        disp('\nSTART DRMC_PC_SEARCH\n')

                                        nparallel = 4;      
                                        REJECPROP = 0;
                                        domixchain = 1;
                                        doinitmixchain = 1;
                                        domodesearch = 1;
                                        useRswitch = 0;  
                                        nrestart = 1;

                                        run_DRMC_P_mog_Search(seed,dim,troff,...
                                                                     REJECPROP,m,maxncompo,...
                                                                     ninitsamp,iterburnin,nrestart,samp4adapt,nsubsamp,...
                                                                     mxit,lfnum,lfsize,itvplot,itvprint,itvsave,PLOT,...
                                                                     mog,hmc,nparallel,runtime,domixchain,doinitmixchain,...
                                                                     useRswitch);                                                          

                                        disp('\nEND DRMC_PC_SEARCH\n')
                                        
                                        
                                case RWHMC_PC_SEARCH

                                        disp('\nSTART RWHMC_PC_SEARCH\n')

                                        nparallel = 4;      
                                        REJECPROP = 0;
                                        domixchain = 1;
                                        domodesearch = 1;
                                        useRswitch = 0;
                                        nrestart = 1;


                                        run_RWHMC_P_mog_Search(seed,dim,...
                                                                     REJECPROP,maxncompo,...
                                                                     iterburnin,nrestart,samp4adapt,nsubsamp,...
                                                                     mxit,itvplot,itvprint,itvsave,PLOT,...
                                                                     mog,w_hmc,nparallel,maxruntime,domixchain,...
                                                                     useRswitch);   

                                        disp('\nEND RWHMC_PC_SEARCH\n')
                                        
                                        

                                case DRMC_PC

                                        disp('\nSTART DRMC_PC\n')

                                        nparallel = 4;      
                                        REJECPROP = 0;
                                        domixchain = 1;
                                        doinitmixchain = 1;
                                        domodesearch = 1;
                                        useRswitch = 0;


                                        run_DRMC_P_mog(seed,dim,troff,...
                                                                     REJECPROP,m,maxncompo,...
                                                                     ninitsamp,iterburnin,nrestart,samp4adapt,nsubsamp,...
                                                                     mxit,lfnum,lfsize,itvplot,itvprint,itvsave,PLOT,...
                                                                     mog,hmc,nparallel,runtime,domixchain,doinitmixchain,...
                                                                     useRswitch);   

                                        disp('\nEND DRMC_PC\n')
                                        
                                        
                                case DRMC_PC_RSWITCH

                                        disp('\nSTART DRMC_PC_RSWITCH\n')

                                        nparallel = 4;      
                                        REJECPROP = 0;
                                        domixchain = 1;
                                        doinitmixchain = 1;
                                        domodesearch = 1;
                                        useRswitch = 1;


                                        run_DRMC_P_mog(seed,dim,troff,...
                                                                     REJECPROP,m,maxncompo,...
                                                                     ninitsamp,iterburnin,nrestart,samp4adapt,nsubsamp,...
                                                                     mxit,lfnum,lfsize,itvplot,itvprint,itvsave,PLOT,...
                                                                     mog,hmc,nparallel,runtime,domixchain,doinitmixchain,...
                                                                     useRswitch);   

                                        disp('\nEND DRMC_PC_RSWITCH\n')


                                case DRMC_P 

                                        disp('\nSTART DRMC_P\n')

                                        nparallel = 1;     
                                        REJECPROP = 0;
                                        domixchain = 0;
                                        doinitmixchain = 1;
                                        domodesearch = 0;
                                        useRswitch = 0;

                                        run_DRMC_P_mog(seed,dim,troff,...
                                                                     REJECPROP,m,maxncompo,...
                                                                     ninitsamp,iterburnin,nrestart,samp4adapt,nsubsamp,...
                                                                     mxit,lfnum,lfsize,itvplot,itvprint,itvsave,PLOT,...
                                                                     mog,hmc,nparallel,runtime,domixchain,doinitmixchain,...
                                                                     domodesearch,useRswitch);

                                        disp('\nEND DRMC_P\n')


                                case DEMC

                                        disp('\nSTART DEMC\n')

                                        nparallel = 4;     
                                        stepsz = 1;
                                        npopu = 20 + 10*k;
                                        noise = 0.0001;

                                        run_DEMC_mog(seed,dim,iterburnin,...
                                                                             mxit,itvplot,itvprint,itvsave,PLOT,mog,nparallel,stepsz,noise,npopu,runtime);

                                        disp('\nEND DEMC\n')


                                case LMCWH

                                        disp('\nSTART LMCWH\n')

                                        nparallel = 1;
                                        NewtonSteps = 6;

                                        run_LMCWH_mog_13(seed,dim,iterburnin,...
                                                                             mxit,itvplot,itvprint,itvsave,PLOT,mog,tunnel,...
                                                                             nparallel,TrajectoryLength,NumOfLeapFrogSteps,NewtonSteps,runtime);

                                        disp('\nEND LMCWT\n')


                                case DMC_PL

                                        disp('\nSTART DMC_PL\n')

                                        nparallel = 4;     
                                        REJECPROP = 5;
                                        domixchain = 0;
                                        doinitmixchain = 1;
                                        domodesearch = 1;
                                        useRswitch = 0;

                                        run_DRMC_P_mog(seed,dim,troff,...
                                                                     REJECPROP,m,maxncompo,...
                                                                     ninitsamp,iterburnin,nrestart,samp4adapt,nsubsamp,...
                                                                     mxit,lfnum,lfsize,itvplot,itvprint,itvsave,PLOT,...
                                                                     mog,hmc,nparallel,runtime,domixchain,doinitmixchain,...
                                                                     useRswitch);   

                                        disp('\nEND DMC_PL\n')


                                case HMC_P


                                        disp('\nSTART HMC_P\n')
                                        itersync = samp4adapt;
                                        iterburnin = 100;
                                        run_HMC_P_mog(seed,dim,iterburnin,...
                                                                     mxit,itvplot,itvprint,itvsave,PLOT,...
                                                                     mog,hmc,nparallel,runtime,itersync)

                                        disp('\nEND HMC_P\n')

                                otherwise

                                        error('undefined algorithm');

                        end

                end

        end

end










                                