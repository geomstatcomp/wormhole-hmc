function SAMP = modesearch_mog(w,nburnin,ninitsamp,llhfunc,gradfunc,hmc,dpgm)

dim = length(w);
s = 0;
SAMP = zeros(dim,ninitsamp);
avgprhmcaccept = 0;

for t = 1 : nburnin + ninitsamp

        [w,~,~,prhmcaccept] = hmcfunc(w, llhfunc, gradfunc, hmc);
        avgprhmcaccept = (1-1/t) * avgprhmcaccept+ 1/t * prhmcaccept;

        if t > nburnin
                s = s + 1;
                SAMP(:,s) = w;
        end                                            

end                        

%% is new mode?
muq = mean(SAMP,2);
if exp(logdpgmpdf(muq',dpgm,100)) > 1e-11
        SAMP = [];
end       