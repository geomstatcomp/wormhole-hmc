clear
close all
% addpath('drawdata');
addpath('../00_utils');
dbstop if error


K = [5 10 15 20];
nK= length(K);
Algo = {'drmc','lmcwh'};
nAlgo = length(Algo);
colors = distinguishable_colors(nAlgo);
Seed = 2013:2022;
nSeed = length(Seed);

files = dir('./00_log_finished/');
nfiles = length(files) - 2;

mc = cell(nAlgo,nK,nSeed);
for ss=1:nSeed
    for kk=1:nK
        for Alg=1:nAlgo
            for ff=1:nfiles
                if ~isempty(strfind(files(ff+2).name,Algo{Alg}))&...
                        ~isempty(strfind(files(ff+2).name,strcat('sd',num2str(Seed(ss)),'_d20_','k',num2str(K(kk)))))
                    mc{Alg,kk,ss} = load(strcat('./00_log_finished/', files(ff+2).name));
                end
            end
        end
    end
end

%% K-R plot
cuttime = 500;
cuttimeR = zeros(nAlgo,nK,nSeed);
cuttimeREM = zeros(nAlgo,nK,nSeed);
iters = zeros(nAlgo,nK,nSeed);

for kk = 1:nK
    for Alg = 1:nAlgo
        for ss=1:nSeed
            ixt = sum(mc{Alg,kk,ss}.TIME < cuttime);
            if ixt > mc{Alg,kk,ss}.p
                   ixt = mc{Alg,kk,ss}.p;
            end
            cuttimeR(Alg,kk,ss) = mc{Alg,kk,ss}.R(ixt); 
            cuttimeREM(Alg,kk,ss) = mc{Alg,kk,ss}.REMU(ixt);
            iters(Alg,kk,ss) = mc{Alg,kk,ss}.s;
        end
    end
end
R.mean = mean(cuttimeR,3); R.std = std(cuttimeR,0,3);
REM.mean = mean(cuttimeREM,3); REM.std = std(cuttimeREM,0,3);

styles = {'-o','--+','-s','--x'};

fig.KREM = figure(241); clf;
% set(fig.KREM,'windowstyle','docked');
for Alg = 1:nAlgo
    errorbar(1:nK,REM.mean(Alg,:),REM.std(Alg,:)./sqrt(nSeed),styles{Alg},'color',colors(Alg,:),'linewidth',2,'markersize',7); hold on;
end
ylim([0,1.1]);
set(gca,'FontSize',15,'xTick',1:nK,'xTickLabel','5|10|15|20');
xlabel('K','FontSize',18); ylabel('REM (after 500 sec)','FontSize',18); 
legend('RDMC','WHMC','FontSize',18,'location','NorthWest');
title('D=20','FontSize',20);


fig.KR = figure(103); clf;
% set(fig.KR,'windowstyle','docked');
% for Alg = 1:nAlgo
%     errorbar(1:nK,R.mean(Alg,:),R.std(Alg,:),styles{Alg},'color',colors(Alg,:),'linewidth',2,'markersize',10); hold on;
% end
R_recal = zeros(nAlgo,nK);
for kk = 1:nK
    for Alg = 1:nAlgo
        sampidx = min(iters(Alg,kk,:));
        cmb_SAMP = [];
        for ss=1:nSeed
            if Alg==1
                cmb_SAMP(:,:,ss) = mc{Alg,kk,ss}.SAMP{1}(:,1:sampidx)';
            else
                cmb_SAMP(:,:,ss) = mc{Alg,kk,ss}.SAMP(:,1:sampidx)';
            end
        end
        R_recal(Alg,kk) = mpsrf(cmb_SAMP);
    end
end

% for Alg = 1:nAlgo
%     plot(1:nK,R_recal(Alg,:),styles{Alg},'color',colors(Alg,:),'linewidth',2,'markersize',7); hold on;
% end
% set(gca,'FontSize',15);
% xlim([.5,nK+.5]);
% set(gca,'xTick',1:nK,'xTickLabel','5|10|15|20');
% ylim([1-.01,1.1]);
% % set(gca,'yTick',[1 1.002 1.004 1.006 1.008 1.01 1.02 1.04 1.06 1.08],'yTickLabel','1|1.002|1.004|1.006|1.008|1.01|1.02|1.04|1.06|1.08');
% xlabel('K','FontSize',18); ylabel('R (after 500 sec)','FontSize',18);
% legend('RDMC','WHMC','FontSize',18,'location','NorthWest');
% title('D=20','FontSize',20);


[AX, H1, H2] = plotyy(1:nK,R_recal(1,:),1:nK,R_recal(2,:));
set(H1,'linestyle','-','color','b','marker','o','linewidth',2);
set(H2,'linestyle','--','color','r','marker','+','linewidth',2);
set(AX(1),'xlim',[.5,nK+.5],'xTick',1:nK,'xTickLabel','5|10|15|20','XColor','b','YColor','b');
set(AX(2),'xlim',[.5,nK+.5],'xTick',1:nK,'xTickLabel','5|10|15|20','XAxisLocation','top','XColor','r','YColor','r');
xlabel('K','FontSize',18); ylabel('R (after 500 sec)','FontSize',18);
ylimits1 = get(AX(1),'YLim');
ylimits2 = get(AX(2),'YLim');
yinc1 = (ylimits1(2)-ylimits1(1))/5; yinc2 = (ylimits2(2)-ylimits2(1))/5;
set(AX(1),'YTick',[ylimits1(1):yinc1:ylimits1(2)]);
set(AX(2),'YTick',[ylimits2(1):yinc2:ylimits2(2)]);

legend('RDMC','WHMC','FontSize',18,'location','NorthWest');
title('D=20','FontSize',20);