Wormhole Hamiltonian Monte Carlo (wormholeHMC) is an HMC algorithm dedicated for
efficiently sampling from multimodal probability distributions.
It exploits and modifies the Riemannian geometric properties of the target 
distribution to create wormholes connecting modes in order to facilitate moving between them.
Further, the method uses the regeneration technique in order to adapt the algorithm 
by identifying new modes and updating the network of wormholes without 
affecting the stationary distribution. To find new modes, as opposed to 
rediscovering those previously identified, we employ a novel mode searching algorithm 
that explores a residual energy function obtained by subtracting an approximate 
Gaussian mixture density (based on previously discovered modes) from the target density function.

This repo contains Matlab codes for examples in the following paper:

Shiwei Lan, Jeffrey Streets, and Babak Shahbaba
Wormhole Hamiltonian Monte Carlo
AAAI 2014, Quebec City, Proceedings of the Twenty-Eighth AAAI Conference on Artificial Intelligence, pp. 1953-1959, July 2014
http://www.aaai.org/ocs/index.php/AAAI/AAAI14/paper/view/8437

CopyRight: Shiwei Lan

These codes are modified from codes of the following work so they are loosely maintained.

Sungjin Ahn, Yutian Chen, Max Welling
Distributed and adaptive darting Monte Carlo through regenerations
In Proceedings of the 16th International Conference on Artificial Intelligence and Statistics (AI Stat), 2013

Please cite the references when using the codes, Thanks!

Shiwei Lan
9-18-2016