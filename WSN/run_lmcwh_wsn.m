function run_lmcwh_wsn (seed,dim,ntotinitsamp,totnburnin,nrestart,...
    mxit,TrajectoryLength,NumOfLeapFrogSteps,NewtonSteps,itvplot,itvprint,itvsave,PLOT)

% clear
% close all
dbstop if error
format compact
addpath('../package/vdpgm');
addpath('../00_drmcmc_core');
addpath('../00_utils');
addpath('./00_wsn_data');

if isdeployed
  seed = str2double(seed);
  dim = str2double(dim);
  ntotinitsamp = str2double(ntotinitsamp);
  totnburnin = str2double(totnburnin);
  mxit = str2double(mxit);
  hmc.lfn = str2double(lfn);
  hmc.lfsize = str2double(lfsize);
  itvplot = str2double(itvplot);
  itvsave = str2double(itvsave);
  itvprint = str2double(itvprint);
  PLOT = str2double(PLOT);
  
  TrajectoryLength   = str2double(TrajectoryLength);
NumOfLeapFrogSteps = str2double(NumOfLeapFrogSteps);
NewtonSteps = str2double(NewtonSteps);  
end
StepSize = TrajectoryLength/NumOfLeapFrogSteps;

nparallel = 1;


%% Init Parallelization
if nparallel > 1

  sz.pool = matlabpool('size');

  if sz.pool > 0 && sz.pool ~= nparallel && nparallel ~= 0

    matlabpool close
    matlabpool(nparallel);

  end

  if sz.pool == 0 && nparallel ~= 0

    matlabpool(nparallel);

  end

end

RandStream.setGlobalStream(RandStream('mt19937ar','Seed',seed));


%% load

% hmc = struct('M', lfn, 'eta', lfsize, 'L',1);

if dim == 6

  ld = load('./00_wsn_data/data_wsn_N6.mat', 'Xs','Xb', 'Yb', 'Ys', 'N', 'R', 'sig');

elseif dim == 8

  ld = load('./00_wsn_data/data_wsn_N8.mat', 'Xs','Xb', 'Yb', 'Ys', 'N', 'R', 'sig');
  gt = load('./00_wsn_data/121105_gt.mat','gt_mean','gt_cov');

end

hmc.Xb = ld.Xb;
hmc.Yb = ld.Yb;
hmc.Ys = ld.Ys;
hmc.N = ld.N;
hmc.R = ld.R;
hmc.sig = ld.sig;

gt.mu = gt.gt_mean;
gt.cov = gt.gt_cov;

%% dim
nnode = ld.N;
dim = ld.N * 2;


global llhfunc gradfunc
%% get function handles
[llhfunc, gradfunc, met] = funs_wsn(ld.Xb, ld.Yb, ld.Ys, ld.N, ld.R, ld.sig);
% clear ld


%% fname
fname = sprintf('wormhole_wsn_seri_d%d_nsinit%d_sd%d',...
  dim,ntotinitsamp,seed);
fname = strrep(fname,'.','-');
fname = appendfilehead(fname);
fname = strcat('_',fname);


%% plots
for j = 1:nparallel
  fig.hist3(j) = figure(440+j); clf;
end

fig.rerror_mu = figure(450); clf;
fig.rerror_cov = figure(451); clf;
fig.R = figure(452); clf;
fig.locerr = figure(453); clf;

if PLOT && ~isdeployed
  for j = 1:nparallel
    set(fig.hist3(j),'windowstyle','docked');
  end
  set(fig.rerror_mu,'windowstyle','docked'); title('REM');
  set(fig.rerror_cov,'windowstyle','docked'); title('REC');
  set(fig.R,'windowstyle','docked'); title('R');
  set(fig.locerr,'windowstyle','docked'); title('LOC ERR');

end

pause(0.5);

% nfig = figrow^2;
nfig = 1;
cvpair = [1:nfig; mod((1:nfig) + dim/2 -1, dim)+1]';

nburnin = ceil(totnburnin / nrestart);
totnburnin = nburnin * nrestart;
ninitsamp =ceil(ntotinitsamp / nrestart);
ntotinitsamp = ninitsamp * nrestart;
mxit = max(mxit, ntotinitsamp);

SAMPBURNIN = zeros(dim, ninitsamp*nrestart*nparallel);
SAMP = zeros(dim, mxit*2, nparallel);
TOUR = zeros(dim, ninitsamp, nparallel);
w = zeros(dim,nparallel);
last_good_w = zeros(dim,nparallel);
avgprhmcaccept = zeros(nparallel,1);
prhmcaccept = zeros(nparallel,1);
% s_burnin = zeros(nparallel,1);
% s = zeros(nparallel,1);

RR = zeros(ceil(mxit/itvplot)+100,dim);
REMU = zeros(ceil(mxit/itvplot)+100,1);
RECV = zeros(ceil(mxit/itvplot)+100,1);
ELOC = zeros(ceil(mxit/itvplot)+100,1);
S = zeros(ceil(mxit/itvplot)+100, 1);
TIME = zeros(ceil(mxit/itvplot)+100,1);

p = 0;
v = 0;

time_start = tic;

% %% mode search
% if domodesearch
% 
%   %preliminary run to build initial dpmm
%   disp('start prerun')
%   std_init = 2;
%   h = 0;
%   ntour = 0;
%   ntotinittour = nparallel*nrestart;
% 
%   while ntour < ntotinittour
% 
%     %                 idx_restart = idx_restart + 1;
% 
%     % Restart
%     w = (rand(dim, nparallel)-.5)*std_init+.5;
%     u = 1;
% 
%     for t = 1 : nburnin + ninitsamp
% 
% %       parfor qq = 1:nparallel  % mode search
%       for qq = 1:nparallel
% 
%         [w(:,qq), ~, prhmcaccept(qq)] = hmc_step_wsn(w(:,qq), hmc);
%         prhmcaccept(qq) = min(1, prhmcaccept(qq));
%         avgprhmcaccept(qq) = (1-1/t) * avgprhmcaccept(qq)+ 1/t * prhmcaccept(qq);
% 
%         if t > nburnin
%           TOUR(:,u,qq) = w(:,qq);
%         end
% 
%         last_good_w(:,qq) = w(:,qq);
% 
%       end
% 
%       itv = itvplot;
% 
%       %% plot
%       if ~rem(t+itv,itv)
%         
%         for qq = 1:min(2, nparallel)
% 
%           fprintf('%d - ntour:%d/%d: Burn-in iter: %d, accept rate: %f\n', qq, ntour, ntotinittour, t, avgprhmcaccept(qq));
%           set(0,'CurrentFigure',fig.hist3(qq)); clf;
% 
%           ss = reshape(TOUR(:,1:u,qq), [nnode, 2, u]);
%           ss = permute(ss,[2 1 3]);
%           ss = ss(:,:);
%           [CU,HU] = hist3image(ss(1,:),ss(2,:),70);
%           imagesc(CU{1},CU{2},-HU); axis xy; colormap hot; hold on;
% 
%           % draw base stations
%           for nn = 1:3
%             plot(hmc.Xb(nn,1),hmc.Xb(nn,2),'ks','linewidth',2,'markersize',10);
%           end
%           % draw node
%           for i = 1:nnode
%             plot(w(i,qq),w(i+nnode,qq),'ro','linewidth',2,'markersize',10);
%             plot(ld.Xs(i,1),ld.Xs(i,2),'kx','linewidth',2,'markersize',10);
%           end
% 
%           hold off
%           axis([-0.1 1 -0.3 1])
%           drawnow
% 
%         end
%       end
% 
%       if t > nburnin
%         u = u + 1;
%       end
% 
%     end
% 
%     %% combine valid tours and stack up in the SAMPBURNIN
%     nvalidtour = sum(avgprhmcaccept > .3);
%     VTOUR = reshape(TOUR(:, :, avgprhmcaccept > .3), [dim ninitsamp*nvalidtour]);
%     assert(size(VTOUR,2) == nvalidtour*ninitsamp);
%     SAMPBURNIN(:,h+1:h+nvalidtour*ninitsamp) = VTOUR(:,:);
%     h = h+nvalidtour*ninitsamp;
%     ntour = ntour + nvalidtour;
% 
%   end
% 
%   %% Set sample index.
%   s_init = ninitsamp*nrestart;
%   s = s_init;
%   SAMP(:, 1:s, :) = reshape(SAMPBURNIN(:,1:s*nparallel), [dim ninitsamp*nrestart nparallel]);
% 
% %   save ../00_wsn_data/prelimrun_4qq_1nrestart.mat t s s_init SAMP last_good_w w avgprhmcaccept
%   save ./00_wsn_data/prelimrun_tmp.mat t s s_init SAMP last_good_w w avgprhmcaccept
%   %         save prelimrun_2qq_allmode.mat t s s_init SAMP last_good_w w avgprhmcaccept idx_restart
%   %         save prelimrun_2qq.mat t s s_init SAMP last_good_w w avgprhmcaccept idx_restart
% 
% 
%   %% plot
%   for qq = 1:min(2, nparallel)
% 
%     fprintf('%d - ntour:%d/%d: Burn-in iter: %d, accept rate: %f\n', qq, ntour, ntotinittour, t, avgprhmcaccept(qq));
%     set(0,'CurrentFigure',fig.hist3(qq)); clf;
% 
%     ss = reshape(SAMP(:,1:s,qq), [nnode, 2, s]);
%     ss = permute(ss,[2 1 3]);
%     ss = ss(:,:);
%     [CU,HU] = hist3image(ss(1,:),ss(2,:),70);
%     imagesc(CU{1},CU{2},-HU); axis xy; colormap hot; hold on;
% 
%     % draw base stations
%     for nn = 1:3
%       plot(hmc.Xb(nn,1),hmc.Xb(nn,2),'ks','linewidth',2,'markersize',10);
%     end
% 
%     for i = 1:nnode
%       plot(ld.Xs(i,1),ld.Xs(i,2),'kx','linewidth',2,'markersize',10);
%       plot(w(i,qq),w(i+nnode,qq),'ro','linewidth',2,'markersize',10);
%     end
% 
%     hold off
%     axis([-0.1 1 -0.3 1])
% 
%     drawnow
% 
%   end
% 
% else
% 
%   %         load prelimrun_2qq
%   %         load prelimrun_2qq_allmode.mat
% %   load ./00_wsn_data/prelimrun_4qq_1nrestart
  load ./00_wsn_data/prelimrun_4qq_allmode.mat
%   %         load 00_wsn_data/burnin__120531_123429_wsn_d16_troff-1-9_reject4_t4adpt50_mxk100_nssub10000_nsinit70000_m2-0_sd2-104772e+05.mat ...
%   %   t s s_init SAMP SAMPBURNIN s_burnin last_good_w w avgprhmcaccept idx_restart
% 
%   if nparallel > size(SAMP,3)
%     warning('nparallel is larger than size(SAMP, 3)');
%   end
%   
  s_init = ninitsamp*nrestart; s = s_init;
  SAMP_1 = SAMP(:, 1:s, mod(0 : nparallel-1, size(SAMP, 3)) + 1);
  SAMP = zeros(dim, mxit*2, nparallel);
  SAMP(:, 1:s, :) = SAMP_1;
  avgprhmcaccept = avgprhmcaccept(mod(0 : nparallel-1, length(avgprhmcaccept)) + 1);
%   last_good_w = last_good_w(:, mod(0 : nparallel-1, size(last_good_w, 2)) + 1);
%   w = w(:, mod(0 : nparallel-1, size(w, 2)) + 1);
  t = nburnin + ninitsamp;
% end
% 
% % combine sample
% 
% CSAMP = reshape(SAMP(:, 1:s, :), [dim, nparallel*ninitsamp*nrestart]);
% lencombsamp = size(CSAMP,2);
% 
% time_prerun = toc(time_start);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mode search
for i=1:size(last_good_w,2)
    [minX(:,i),minf,exitflag(i),~,~,Hess(:,:,i)] = fminunc(@(x)-llhfunc(x),last_good_w(:,i));
end
mode = minX(:,exitflag==1);
H = Hess(:,:,exitflag==1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sampling
% wcurr = zeros(dim, nparallel);
% xcurr = SAMP(:,s,:);
xcurr = zeros(dim,1);
t = 0;



avgmhratio = 0;
numaccept = 0;
numjump = 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% tunnel
tunnel.modes = mode;
[~,k] = size(tunnel.modes);
tunnel.length = zeros(k); tunnel.width = zeros(k);
tunnel.upvec = cell(k); tunnel.dnvec = cell(k);
tunnel.upmet = cell(k); tunnel.dnmet = cell(k);
for i=1:k
    [V,D]=eig(H(:,:,i));
    for j=i:k
        tunnel.length(i,j) = norm([tunnel.modes(:,j)-tunnel.modes(:,i); 2]); % extra-dimension 2=1-(-1);
        upvec = [tunnel.modes(:,j)-tunnel.modes(:,i); 2]./tunnel.length(i,j); tunnel.upvec{i,j} = upvec;
        dnvec = upvec; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
        tunnel.width(i,j) = 2*D(end,end)*norm(V(:,end)-upvec(1:dim).*(upvec(1:dim)'*V(:,end)));
        tunnel.upmet{i,j} = eye(dim+1)-(1-StepSize/20).*(upvec*upvec');
        tunnel.dnmet{i,j} = eye(dim+1)-(1-StepSize/20).*(dnvec*dnvec');
    end
    for j=1:i-1
        tunnel.length(i,j) = tunnel.length(j,i);
        upvec = -tunnel.upvec{j,i}; upvec(end)=-upvec(end); tunnel.upvec{i,j} = upvec;
        dnvec = -tunnel.dnvec{j,i}; dnvec(end)=-dnvec(end); tunnel.dnvec{i,j} = dnvec;
        tunnel.width(i,j) = 2*D(end,end)*norm(V(:,end)-upvec(1:dim).*(upvec(1:dim)'*V(:,end)));
        tunnel.upmet{i,j} = eye(dim+1)-(1-StepSize/20).*(upvec*upvec');
        tunnel.dnmet{i,j} = eye(dim+1)-(1-StepSize/20).*(dnvec*dnvec');
    end
end
tunnel.influence = 8e-6;
tunnel.width = tunnel.influence.*tunnel.width;


xcurr = [xcurr;-1];

tic;

% prep: energy, G, dG, etc
CurrentU = U(xcurr);
% Calculate G
[G InvG dG Gamma1] = met(xcurr,[0 -1 1 3]);
CholInvG = chol(InvG);
% Calculate the partial derivatives dG/dq
for d = 1:dim+1
    TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
end
% terms other than quadratic one
dphi = U(xcurr,1) + 0.5*TraceInvGdG';

while t < mxit

  t = t + 1;
  s = s + 1;
  
  xNew = xcurr;
  GNew = G; CholInvGNew = CholInvG; Gamma1New = Gamma1;
  dphiNew = dphi;

  % propose velocity
  Velocity = (randn(1,dim+1)*CholInvGNew)';

  % Calculate current H value
  CurrentLogDet = sum(log(diag(CholInvGNew)));
  CurrentH = CurrentU + CurrentLogDet + (Velocity'*GNew*Velocity)/2;

  % Accumulate determinant to be adjusted in acceptance rate
  Deltalogdet = 0;

  jumped = 0; adjusted = 0; EnergyAdj = 0;
  % Perform leapfrog steps
for StepNum = 1:NumOfLeapFrogSteps

    %%%%%%%%%%%%%%%%%%%
    % Update velocity %
    %%%%%%%%%%%%%%%%%%%
  % Make a half step for Velocity
    VGamma1 = zeros(dim+1);
    for kk=1:dim+1
        VGamma1(kk,:) = Velocity'*Gamma1New(:,:,kk);
    end
    Deltalogdet = Deltalogdet - log(det(GNew+(StepSize/2)*VGamma1));
    Velocity = (GNew+(StepSize/2)*VGamma1)\(GNew*Velocity- (StepSize/2)*dphiNew);
    for kk=1:dim+1
        VGamma1(kk,:) = Velocity'*Gamma1New(:,:,kk);
    end
    Deltalogdet = Deltalogdet + log(det(GNew-(StepSize/2)*VGamma1));


    %%%%%%%%%%%%%%%%%%%%%%%
    % Update q parameters %
    %%%%%%%%%%%%%%%%%%%%%%%
    % Make a full step for the position
    if jumped
        xNew = xNew + StepSize.*Velocity;
    else
        xTemp = xNew;
        [~,closest] = min(sum((repmat(xTemp(1:dim),1,k)-tunnel.modes).^2));
        tnl.length = tunnel.length(closest,:);
        if xTemp(end)<0
            tnl.L = [tunnel.modes(:,closest); -1]; tnl.R = [tunnel.modes; ones(1,k)];
            tnl.vec = cat(2,tunnel.upvec{closest,:});
        else
            tnl.L = [tunnel.modes(:,closest); 1]; tnl.R = [tunnel.modes; -ones(1,k)];
            tnl.vec = cat(2,tunnel.dnvec{closest,:});
        end
        for FixedPointIteration = 1:NewtonSteps

            X2tnlL = repmat(xTemp-tnl.L,1,k); X2tnlR = repmat(xTemp,1,k)-tnl.R;
            projL = sum(X2tnlL.*tnl.vec); projR = sum(X2tnlR.*tnl.vec);
            vicinity = sum(X2tnlL.*X2tnlR) + abs(projL.*projR);
            moll = exp(-vicinity/dim./tunnel.width(closest,:));
            if rand<1-sum(moll)
                Move = Velocity;
            else
                jump2 = randsample(k,1,true,moll);
%                 disp(['Jumping to mode ' num2str(jump2)]);
                Move = -2/StepSize.*X2tnlR(:,jump2);
                jumped = 1; adjprob = min([1,sum(moll)]);
            end
            if FixedPointIteration==1
                Movefixed = Move;
            end

            xTemp = xNew + .5*StepSize.*(Movefixed+Move);
        end
            
        [~,closestNew] = min(sum((repmat(xTemp(1:dim),1,k)-tunnel.modes).^2));
        if closestNew~=closest&jumped
            EnergyAdj = U(xTemp) - (U(xNew)+(Velocity'*Velocity)/2);
            adjusted = 1;
            numjump = numjump +1;
        end
        
        xNew = xTemp;
    end

    % Update G based on new parameters
    [GNew InvG dG Gamma1New] = met(xNew,[0 -1 1 3]);
    % Update the partial derivatives dG/dq
    for d = 1:dim+1
        TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
    end
    % terms other than quadratic one
    dphiNew = U(xNew,1) + 0.5*TraceInvGdG';

    %%%%%%%%%%%%%%%%%%%
    % Update velocity %
    %%%%%%%%%%%%%%%%%%%
    % Make a half step for Velocity
    for kk=1:dim+1
        VGamma1(kk,:) = Velocity'*Gamma1New(:,:,kk);
    end
    Deltalogdet = Deltalogdet - log(det(GNew+(StepSize/2)*VGamma1));
    Velocity = (GNew+(StepSize/2)*VGamma1)\(GNew*Velocity- (StepSize/2)*dphiNew);
    for kk=1:dim+1
        VGamma1(kk,:) = Velocity'*Gamma1New(:,:,kk);
    end
    Deltalogdet = Deltalogdet + log(det(GNew-(StepSize/2)*VGamma1));
    
    if adjusted
        EnergyAdj = EnergyAdj + (Velocity'*Velocity)/2;
%         EnergyAdj = adjprob*EnergyAdj;
        adjusted = 0;
    end

end

    try
        % Calculate proposed H value
        ProposedU = U(xNew); CholInvGNew = chol(InvG);
        ProposedLogDet = sum(log(diag(CholInvGNew)));
        ProposedH = ProposedU + ProposedLogDet + (Velocity'*GNew*Velocity)/2;

        % Accept according to ratio
        ratio = exp(-ProposedH + CurrentH + real(Deltalogdet) + EnergyAdj);
        avgmhratio = (1-1/t)*avgmhratio + (1/t)*ratio;

        if ratio > 1 || rand < ratio
            xcurr = xNew; CurrentU = ProposedU;
            G = GNew; CholInvG = CholInvGNew; Gamma1 = Gamma1New;
            dphi = dphiNew;
            numaccept = numaccept + 1;
        end
    catch

    end
  
  
  SAMP(:,s) = xcurr(1:dim);

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% save
%   if ~rem(t+itvsave,itvsave) && p > 0
% 
%     v = v + 1;
% 
%     cd 00_log_working

    % PNG
    %                 saveas(fig.hist3(1),strcat(fname,'_HIST1.png'),'png');
    %                 saveas(fig.hist3(2),strcat(fname,'_HIST2.png'),'png');
    %                 saveas(fig.R,strcat(fname,'_R.png'),'png');
    %                 saveas(fig.rerror_mu,strcat(fname,'_REMU.png'),'png');
    %                 saveas(fig.rerror_cov,strcat(fname,'_RECV.png'),'png');
    %
    %                 % FIG
    %                 saveas(fig.hist3(1),strcat(fname,'_HIST1.fig'),'fig');
    %                 saveas(fig.hist3(2),strcat(fname,'_HIST2.fig'),'fig');
    %                 saveas(fig.R,strcat(fname,'_R.fig'),'fig');
    %                 saveas(fig.rerror_mu,strcat(fname,'_REMU.fig'),'fig');
    %                 saveas(fig.rerror_cov,strcat(fname,'_RECV.fig'),'fig');

%     save(fname);
%     % save(fname,'SAMP','s','s_init','seed','dim','lfn','lfsize','nrestart','ninitsamp','nburnin','nsubsamp');
% 
%     disp('---------------- SAVE COMPLETE! ----------------');
%     cd ..
% 
%   end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% print
  if ~rem(t+itvprint,itvprint)

    %                 fname
    % elapsed
    %                 iterafteradapt
    %[ratehopaccept(nadapt(qq)+1) rateregen(nadapt(qq)+1)]
    avgmhratio
    lmcwhacpt = numaccept/t
    jumprat = numjump/t
    t

%     for qq=1:min(2, nparallel)
% 
% %       sprintf('qq:%d\n',qq)
%       sprintf('t:%d, s:%d, acceptrate:%f',t,s,numaccept/t);
% 
%     end

  end


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% plot

  if ~rem(t+itvplot,itvplot)

    p = p + 1;
    
    S(p) = s;
    TIME(p) = toc(time_start);

    %% R convergence Diagnostic
    if s > nburnin

      % compute R convergence diagnostic
      RR(p) = mpsrf(permute(SAMP(:,s_init+1:s,:), [2 1 3]));
      
      %% compute error
      for qq=1:nparallel

        sampmu = mean(SAMP(:,s_init+1:s,qq)');
        sampcov = cov(SAMP(:,s_init+1:s,qq)');
        ELOC(p,qq) = sum(abs(ld.Xs(:)' - sampmu));

        type = '-k';
        REMU(p,qq) = sum(abs(gt.mu - sampmu))/sum(abs(gt.mu));
        set(0, 'currentfigure', fig.rerror_mu);
%         plot(1:p, REMU(1:p,qq), type);
        plot(TIME(1:p), REMU(1:p,qq), type); title('REM');
        
        RECV(p,qq) = sum(sum(abs(gt.cov - sampcov)))/sum(sum(abs(gt.cov)));
        set(0, 'currentfigure', fig.rerror_cov);
%         plot(1:p, RECV(1:p,qq), type);
        plot(TIME(1:p), RECV(1:p,qq), type); title('REC');
        
        set(0, 'currentfigure', fig.R);
%         plot(1:p, RR(1:p), type);
        plot(TIME(1:p), RR(1:p), type); title('R');
        
        set(0, 'currentfigure', fig.locerr);
%         plot(1:p, ELOC(1:p,qq) , type);
        plot(TIME(1:p), ELOC(1:p,qq) , type); title('ELOC');

      end

    end

    drawnow;

    %% plot

    for qq=1:nparallel

      set(0,'currentfigure',fig.hist3(qq)); clf

      ss = reshape(SAMP(:,s_init+1:s,qq), [nnode, 2, s-s_init]);
      ss = permute(ss,[2 1 3]);
      ss = ss(:,:);
      [CU,HU] = hist3image(ss(1,:),ss(2,:),70);
      imagesc(CU{1},CU{2},-HU); axis xy; colormap hot; hold on;

      % draw base stations
      for nn = 1:3
        plot(hmc.Xb(nn,1),hmc.Xb(nn,2),'ks','linewidth',2,'markersize',10);
      end
      
      for i = 1:nnode

        plot(ld.Xs(i,1),ld.Xs(i,2),'kx','linewidth',2,'markersize',10);
        plot(xcurr(i,qq),xcurr(i+nnode,qq),'ro','linewidth',2,'markersize',10);

      end

      hold off
      axis([-0.1 1 -0.3 1])
      % axis(axax(ff,:));

      title(strrep(fname,'_','-'));
      drawnow;

    end

    toc
    tic

  end
end

%% save when finished
fname = fname(2:end);
cd 00_log_finished

% saveas(fig.hist3(1),strcat(fname,'_HIST1.png'),'png');
% saveas(fig.hist3(2),strcat(fname,'_HIST2.png'),'png');
% saveas(fig.R,strcat(fname,'_R.png'),'png');
% saveas(fig.rerror_mu,strcat(fname,'_REMU.png'),'png');
% saveas(fig.rerror_cov,strcat(fname,'_RECV.png'),'png');
%
% saveas(fig.hist3(1),strcat(fname,'_HIST1.fig'),'fig');
% saveas(fig.hist3(2),strcat(fname,'_HIST2.fig'),'fig');
% saveas(fig.R,strcat(fname,'_R.fig'),'fig');
% saveas(fig.rerror_mu,strcat(fname,'_REMU.fig'),'fig');
% saveas(fig.rerror_cov,strcat(fname,'_RECV.fig'),'fig');

save(fname);
cd ..
disp('---------------- RUN COMPLETE! ----------------');
% save(fname,'SAMP','s','s_init','seed','dim','lfnum','lfsize','nrestart','ninitsamp','iterburnin','nsubsamp');
