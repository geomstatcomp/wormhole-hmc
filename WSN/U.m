function [output] = U(x,der)
if(nargin<2)
    der=0;
end

global llhfunc gradfunc

if der==0
    output = -llhfunc(x(1:size(x,1)-1,:)');
    output = output + .5*x(end)^2;
elseif der==1
    output = -gradfunc(x(1:size(x,1)-1,:));
    output = [output; x(end)];
else
    disp('wrong choice of der!');
end

end