function R = convdiag_R(SAMP,s)

if iscell(SAMP)
  dim = size(SAMP{1}, 1);
  numchain = length(SAMP);

  for dd = 1:dim

    for qq = 1:numchain

      wvar(qq) = var(SAMP{qq}(dd,1:s));
      wmu(qq) = mean(SAMP{qq}(dd,1:s));

    end

    WV(dd) = mean(wvar);
    BV(dd) = s*var(wmu);
    V(dd) = (1-1/s)*WV(dd) + (1/s)*BV(dd);
    R(dd) = sqrt(V(dd)/WV(dd));

  end
else
  dim = size(SAMP, 1);
  numchain = size(SAMP, 3);

  for dd = 1:dim

    for qq = 1:numchain

      wvar(qq) = var(SAMP(dd,1:s,qq));
      wmu(qq) = mean(SAMP(dd,1:s,qq));

    end

    WV(dd) = mean(wvar);
    BV(dd) = s*var(wmu);
    V(dd) = (1-1/s)*WV(dd) + (1/s)*BV(dd);
    R(dd) = sqrt(V(dd)/WV(dd));

  end
end