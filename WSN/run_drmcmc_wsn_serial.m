function run_drmcmc_wsn_serial (seed,dim,troff,REJECPROP,m,maxncompo,...
  ntotinitsamp,totnburnin,nrestart,SAMP4ADAPT,nsubsamp,mxit,lfn,lfsize,itvplot,itvprint,itvsave,PLOT)

% clear
% close all
dbstop if error
format compact
addpath('../package/vdpgm');
addpath('../00_drmcmc_core');
addpath('../00_utils');
addpath('./00_wsn_data');

if isdeployed
  seed = str2double(seed);
  dim = str2double(dim);
  troff = str2double(troff);
  tgradius = sqrt(dim-1) + troff;
  REJECPROP = str2double(REJECPROP);
  m = str2double(m);
  maxncompo = str2double(maxncompo);
  ntotinitsamp = str2double(ntotinitsamp);
  totnburnin = str2double(totnburnin);
  SAMP4ADAPT = str2double(SAMP4ADAPT);
  nsubsamp = str2double(nsubsamp);
  mxit = str2double(mxit);
  hmc.lfn = str2double(lfn);
  hmc.lfsize = str2double(lfsize);
  itvplot = str2double(itvplot);
  itvsave = str2double(itvsave);
  itvprint = str2double(itvprint);
  PLOT = str2double(PLOT);
end

nparallel = 1;
dpgmalgo = 1;
domixchain = 0;
domodesearch = 1;
samp4adapt_inc = 1.1;
trquant = .5;

%% Init Parallelization
if nparallel > 1

  sz.pool = matlabpool('size');

  if sz.pool > 0 && sz.pool ~= nparallel && nparallel ~= 0

    matlabpool close
    matlabpool(nparallel);

  end

  if sz.pool == 0 && nparallel ~= 0

    matlabpool(nparallel);

  end

end

RandStream.setGlobalStream(RandStream('mt19937ar','Seed',seed));


%% load

hmc = struct('M', lfn, 'eta', lfsize, 'L',1);

if dim == 6

  ld = load('./00_wsn_data/data_wsn_N6.mat', 'Xs','Xb', 'Yb', 'Ys', 'N', 'R', 'sig');

elseif dim == 8

  ld = load('./00_wsn_data/data_wsn_N8.mat', 'Xs','Xb', 'Yb', 'Ys', 'N', 'R', 'sig');
  gt = load('./00_wsn_data/121105_gt.mat','gt_mean','gt_cov');

end

hmc.Xb = ld.Xb;
hmc.Yb = ld.Yb;
hmc.Ys = ld.Ys;
hmc.N = ld.N;
hmc.R = ld.R;
hmc.sig = ld.sig;

gt.mu = gt.gt_mean;
gt.cov = gt.gt_cov;

%% dim
nnode = ld.N;
dim = ld.N * 2;
tgradius = ones(nparallel, 1) * (sqrt(dim-1) + troff);


%% get function handles
[llhfunc, gradfunc] = funs_wsn(ld.Xb, ld.Yb, ld.Ys, ld.N, ld.R, ld.sig);
% clear ld


%% fname
fname = sprintf('wsn_seri_d%d_troff%2.1f_reject%d_s4adpt%d_mxk%d_nssub%d_nsinit%d_m%1.1f_sd%d',...
  dim,troff,REJECPROP,SAMP4ADAPT,maxncompo,nsubsamp,ntotinitsamp,m,seed);
fname = strrep(fname,'.','-');
fname = appendfilehead(fname);
fname = strcat('_',fname);


%% plots
for j = 1:nparallel
  fig.hist3(j) = figure(440+j); clf;
end

fig.rerror_mu = figure(450); clf;
fig.rerror_cov = figure(451); clf;
fig.R = figure(452); clf;
fig.locerr = figure(453); clf;

if PLOT && ~isdeployed
  for j = 1:nparallel
    set(fig.hist3(j),'windowstyle','docked');
  end
  set(fig.rerror_mu,'windowstyle','docked'); title('REM');
  set(fig.rerror_cov,'windowstyle','docked'); title('REC');
  set(fig.R,'windowstyle','docked'); title('R');
  set(fig.locerr,'windowstyle','docked'); title('LOC ERR');

end

pause(0.5);

% nfig = figrow^2;
nfig = 1;
cvpair = [1:nfig; mod((1:nfig) + dim/2 -1, dim)+1]';

nburnin = ceil(totnburnin / nrestart);
totnburnin = nburnin * nrestart;
ninitsamp =ceil(ntotinitsamp / nrestart);
ntotinitsamp = ninitsamp * nrestart;
mxit = max(mxit, ntotinitsamp);

SAMPBURNIN = zeros(dim, ninitsamp*nrestart*nparallel);
SAMP = zeros(dim, mxit*2, nparallel);
TOUR = zeros(dim, ninitsamp, nparallel);
INTG = zeros(mxit*2, nparallel);
w = zeros(dim,nparallel);
last_good_w = zeros(dim,nparallel);
avgprhmcaccept = zeros(nparallel,1);
prhmcaccept = zeros(nparallel,1);
% s_burnin = zeros(nparallel,1);
% s = zeros(nparallel,1);

RR = zeros(ceil(mxit/itvplot)+100,dim);
REMU = zeros(ceil(mxit/itvplot)+100,1);
RECV = zeros(ceil(mxit/itvplot)+100,1);
ELOC = zeros(ceil(mxit/itvplot)+100,1);
S = zeros(ceil(mxit/itvplot)+100, 1);
TIME = zeros(ceil(mxit/itvplot)+100,1);

p = 0;
v = 0;

time_start = tic;

%% mode search
if domodesearch

  %preliminary run to build initial dpmm
  disp('start prerun')
  std_init = 2;
  h = 0;
  ntour = 0;
  ntotinittour = nparallel*nrestart;

  while ntour < ntotinittour

    %                 idx_restart = idx_restart + 1;

    % Restart
    w = (rand(dim, nparallel)-.5)*std_init+.5;
    u = 1;

    for t = 1 : nburnin + ninitsamp

%       parfor qq = 1:nparallel  % mode search
      for qq = 1:nparallel

        [w(:,qq), ~, prhmcaccept(qq)] = hmc_step_wsn(w(:,qq), hmc);
        prhmcaccept(qq) = min(1, prhmcaccept(qq));
        avgprhmcaccept(qq) = (1-1/t) * avgprhmcaccept(qq)+ 1/t * prhmcaccept(qq);

        if t > nburnin
          TOUR(:,u,qq) = w(:,qq);
        end

        last_good_w(:,qq) = w(:,qq);

      end

      itv = itvplot;

      %% plot
      if ~rem(t+itv,itv)
        
        for qq = 1:min(2, nparallel)

          fprintf('%d - ntour:%d/%d: Burn-in iter: %d, accept rate: %f\n', qq, ntour, ntotinittour, t, avgprhmcaccept(qq));
          set(0,'CurrentFigure',fig.hist3(qq)); clf;

          ss = reshape(TOUR(:,1:u,qq), [nnode, 2, u]);
          ss = permute(ss,[2 1 3]);
          ss = ss(:,:);
          [CU,HU] = hist3image(ss(1,:),ss(2,:),70);
          imagesc(CU{1},CU{2},-HU); axis xy; colormap hot; hold on;

          % draw base stations
          for nn = 1:3
            plot(hmc.Xb(nn,1),hmc.Xb(nn,2),'ks','linewidth',2,'markersize',10);
          end
          % draw node
          for i = 1:nnode
            plot(w(i,qq),w(i+nnode,qq),'ro','linewidth',2,'markersize',10);
            plot(ld.Xs(i,1),ld.Xs(i,2),'kx','linewidth',2,'markersize',10);
          end

          hold off
          axis([-0.1 1 -0.3 1])
          drawnow

        end
      end

      if t > nburnin
        u = u + 1;
      end

    end

    %% combine valid tours and stack up in the SAMPBURNIN
    nvalidtour = sum(avgprhmcaccept > .3);
    VTOUR = reshape(TOUR(:, :, avgprhmcaccept > .3), [dim ninitsamp*nvalidtour]);
    assert(size(VTOUR,2) == nvalidtour*ninitsamp);
    SAMPBURNIN(:,h+1:h+nvalidtour*ninitsamp) = VTOUR(:,:);
    h = h+nvalidtour*ninitsamp;
    ntour = ntour + nvalidtour;

  end

  %% Set sample index.
  s_init = ninitsamp*nrestart;
  s = s_init;
  SAMP(:, 1:s, :) = reshape(SAMPBURNIN(:,1:s*nparallel), [dim ninitsamp*nrestart nparallel]);

%   save ../00_wsn_data/prelimrun_4qq_1nrestart.mat t s s_init SAMP last_good_w w avgprhmcaccept
  save ./00_wsn_data/prelimrun_tmp.mat t s s_init SAMP last_good_w w avgprhmcaccept
  %         save prelimrun_2qq_allmode.mat t s s_init SAMP last_good_w w avgprhmcaccept idx_restart
  %         save prelimrun_2qq.mat t s s_init SAMP last_good_w w avgprhmcaccept idx_restart


  %% plot
  for qq = 1:min(2, nparallel)

    fprintf('%d - ntour:%d/%d: Burn-in iter: %d, accept rate: %f\n', qq, ntour, ntotinittour, t, avgprhmcaccept(qq));
    set(0,'CurrentFigure',fig.hist3(qq)); clf;

    ss = reshape(SAMP(:,1:s,qq), [nnode, 2, s]);
    ss = permute(ss,[2 1 3]);
    ss = ss(:,:);
    [CU,HU] = hist3image(ss(1,:),ss(2,:),70);
    imagesc(CU{1},CU{2},-HU); axis xy; colormap hot; hold on;

    % draw base stations
    for nn = 1:3
      plot(hmc.Xb(nn,1),hmc.Xb(nn,2),'ks','linewidth',2,'markersize',10);
    end

    for i = 1:nnode
      plot(ld.Xs(i,1),ld.Xs(i,2),'kx','linewidth',2,'markersize',10);
      plot(w(i,qq),w(i+nnode,qq),'ro','linewidth',2,'markersize',10);
    end

    hold off
    axis([-0.1 1 -0.3 1])

    drawnow

  end

else

  %         load prelimrun_2qq
  %         load prelimrun_2qq_allmode.mat
%   load ./00_wsn_data/prelimrun_4qq_1nrestart
  load ./00_wsn_data/prelimrun_4qq_allmode.mat
  %         load 00_wsn_data/burnin__120531_123429_wsn_d16_troff-1-9_reject4_t4adpt50_mxk100_nssub10000_nsinit70000_m2-0_sd2-104772e+05.mat ...
  %   t s s_init SAMP SAMPBURNIN s_burnin last_good_w w avgprhmcaccept idx_restart

  if nparallel > size(SAMP,3)
    warning('nparallel is larger than size(SAMP, 3)');
  end
  
  s_init = ninitsamp*nrestart; s = s_init;
  SAMP_1 = SAMP(:, 1:s, mod(0 : nparallel-1, size(SAMP, 3)) + 1);
  SAMP = zeros(dim, mxit*2, nparallel);
  SAMP(:, 1:s, :) = SAMP_1;
  avgprhmcaccept = avgprhmcaccept(mod(0 : nparallel-1, length(avgprhmcaccept)) + 1);
  last_good_w = last_good_w(:, mod(0 : nparallel-1, size(last_good_w, 2)) + 1);
  w = w(:, mod(0 : nparallel-1, size(w, 2)) + 1);
  t = nburnin + ninitsamp;
end

% combine sample

CSAMP = reshape(SAMP(:, 1:s, :), [dim, nparallel*ninitsamp*nrestart]);
lencombsamp = size(CSAMP,2);

time_prerun = toc(time_start);

%% first dpgm update
% ixsubsamp = cell(nparallel,1);

% parfor qq=1:nparallel  % first dpgm update
for qq=1:nparallel

  if domixchain

% %     if lencombsamp < nsubsamp
% %       % ixsubsamp = ceil(rand(1,lencombsamp)*lencombsamp);
% %       ixsubsamp = 1:lencombsamp;
% %     else
% %       % ixsubsamp = ceil(rand(1,nsubsamp)*lencombsamp);
% %       % thinning
% % %       ixsubsamp = qq:ceil((lencombsamp-qq)/nsubsamp):lencombsamp;
% %       ixsubsamp = round(linspace(1, lencombsamp, nsubsamp));
% %     end

%     dpgm(qq) = dpgaussmixture(CSAMP(:,ixsubsamp),maxncompo,dpgmalgo);
    if REJECPROP ~= 5
%       dpgm(qq) = dpgaussmixture(CSAMP(:,ixsubsamp),nsubsamp,maxncompo,dpgmalgo,REJECPROP == 4);
      dpgm(qq) = dpgaussmixture_Yutian(CSAMP,nsubsamp,maxncompo,dpgmalgo,REJECPROP == 4);
    else
      dpgm(qq) = dpgmLaplace('../../data/data_wsn_N8_modes.mat');
      dpgm(qq).ixsub = ceil(linspace(1, lencombsamp, min(nsubsamp, lencombsamp)));
    end
    
    % Choose tgradius
    [~,dist_tg_vec] = isinregionL(CSAMP(:,dpgm(qq).ixsub), dpgm(qq).mu, dpgm(qq).L, tgradius(qq));
    dhist = min(dist_tg_vec, [], 1);
    tgradius(qq) = quantile(dhist, trquant);

    offset(qq) = getmodeoffset(llhfunc,dpgm(qq),tgradius(qq));
%     maxfreqlogwx(qq) = histwx(CSAMP(:,ixsubsamp)',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);
    maxfreqlogwx(qq) = histwx(CSAMP(:,dpgm(qq).ixsub)',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);

  else

% %     if s < nsubsamp
% %       % ixsubsamp = ceil(rand(1,lencombsamp)*lencombsamp);
% %       ixsubsamp = 1:s;
% %     else
% %       % ixsubsamp = ceil(rand(1,nsubsamp)*lencombsamp);
% %       % thinning
% % %       ixsubsamp = qq:ceil((s-qq)/nsubsamp):s;
% %       ixsubsamp = round(linspace(1, s, nsubsamp));
% %     end

%     dpgm(qq) = dpgaussmixture(SAMP(:,ixsubsamp,qq),maxncompo,dpgmalgo);
    if REJECPROP ~= 5
%       dpgm(qq) = dpgaussmixture(SAMP(:,ixsubsamp,qq),nsubsamp,maxncompo,dpgmalgo,REJECPROP == 4);
      dpgm(qq) = dpgaussmixture_Yutian(SAMP(:,1:s,qq),nsubsamp,maxncompo,dpgmalgo,REJECPROP == 4);
    else
      dpgm(qq) = dpgmLaplace('../../data/data_wsn_N8_modes.mat');
      dpgm(qq).ixsub = ceil(linspace(1, s, min(nsubsamp, s)));
    end

    % Choose tgradius
    [~,dist_tg_vec] = isinregionL(SAMP(:,dpgm(qq).ixsub,qq), dpgm(qq).mu, dpgm(qq).L, tgradius(qq));
    dhist = min(dist_tg_vec, [], 1);
    tgradius(qq) = quantile(dhist, trquant);

    offset(qq) = getmodeoffset(llhfunc,dpgm(qq),tgradius(qq));
%     maxfreqlogwx(qq) = histwx(SAMP(:,ixsubsamp,qq)',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);
    maxfreqlogwx(qq) = histwx(SAMP(:,dpgm(qq).ixsub,qq)',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);

  end

end

disp('first dpmm update done!')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sampling
% wcurr = zeros(dim, nparallel);
wcurr = SAMP(:,s,:);
t = 0;
% s = 0; % Do not reset s.
% SAMP(:,s,:) = wcurr;
% ISREGEN = zeros(1,2*mxit);
% REGEN_S = zeros(1,ceil(mxit/20));
% rateregen = zeros(1,ceil(mxit/20));
% rateregen2 = zeros(1,ceil(mxit/20));
% ratehopaccept = zeros(1, ceil(mxit/20));
% ratehopaccept2 = zeros(1,ceil(mxit/20));
% iteratadapt = zeros(1, ceil(mxit/20));
% itaftadapt = zeros(1,ceil(mxit/20));
% timeatadapt = zeros(1, ceil(mxit/20));
% NREJECT = zeros(size(REGEN_S));
hmcaccept = zeros(1, nparallel);
nhmcaccept = zeros(1, nparallel);
whmc = zeros(dim, nparallel);
isintg = false(nparallel, 1);

% return from independent sampler
wdpgm = zeros(dim, nparallel);
ishopaccept = zeros(1, nparallel);
prhop_t = zeros(1, nparallel);
isregen = zeros(1, nparallel);
prregen_t = zeros(1, nparallel);
nreject = zeros(1, nparallel);
nreject_t = zeros(1, nparallel);
nhopaccept = zeros(1, nparallel);
sumprregen = zeros(1, nparallel);
sumprhopaccept = zeros(1, nparallel);
nindepsamp = zeros(1, nparallel);
nadapt = zeros(1, nparallel);
nhmc = zeros(1, nparallel);
nregen = zeros(1, nparallel);
nintg = zeros(1, nparallel);
% iterafteradapt = zeros(1, nparallel);
iterlastregen = zeros(1,nparallel);
% iterafteradapt = zeros(1,nparallel);
% ntours = zeros(1,nparallel);
% iterlastregen = codistributed.zeros(1, nparallel);
lastupdate = ones(1, nparallel) * s;
samp4adapt = ones(1, nparallel) * SAMP4ADAPT;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;

while t < mxit

  t = t + 1;
  s = s + 1;

%   parfor qq=1:nparallel  % main loop
  for qq=1:nparallel

    % 1st KERNEL, HMC

    [whmc(:,qq), hmcaccept(qq)] = hmc_step_wsn(wcurr(:,qq), hmc);
    %                 [whmc(:,qq), hmcaccept(qq)] = sample_RWM_wsn(wcurr(:,qq), 0.0001, llhfunc);
    nhmc(qq) = nhmc(qq) + 1;
    nhmcaccept(qq) = nhmcaccept(qq) + hmcaccept(qq);
%     isintg(qq) = any(isinregion(whmc(:,qq)',dpgm(qq).mu,dpgm(qq).cv,tgradius(qq)));
    isintg(qq) = any(isinregionL(whmc(:,qq),dpgm(qq).mu,dpgm(qq).L,tgradius(qq)));
    nintg(qq) = nintg (qq)+ isintg(qq);
    INTG(s,qq) = isintg(qq);
    
    wcurr(:,qq) = whmc(:,qq);
    
    % 2nd KERNEL, DPMM independent sampler
    if isintg(qq)

      % propose from dpmm
      [wdpgm(:,qq),ishopaccept(qq),prhop_t(qq),isregen(qq),prregen_t(qq),nreject_t(qq)] = ...
        indepsampler_dpgm_Yutian(...
        whmc(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),REJECPROP,tgradius(qq));
      
      % compute some statistics
      nreject(qq) = nreject(qq) + nreject_t(qq);
      nhopaccept(qq) = nhopaccept(qq) + ishopaccept(qq);
      nindepsamp(qq) = nindepsamp(qq) + 1;
      nregen(qq) = nregen(qq) + isregen(qq);
      sumprregen(qq) = sumprregen(qq) + prregen_t(qq);
      sumprhopaccept(qq) = sumprhopaccept(qq) + prhop_t(qq);

      if ishopaccept(qq)

        wcurr(:,qq) = wdpgm(:,qq);

      end
      
    end  % if isintg(qq)
    
    SAMP(:,s,qq) = wcurr(:,qq);

  end

  % combine new tours
  if domixchain
    
    for qq = find(isintg & ishopaccept & isregen)
      
      ixnewtour = (iterlastregen(qq)+1):s;
      CSAMP = [CSAMP SAMP(:,ixnewtour,qq)];

    end

    lencombsamp = size(CSAMP,2);

  end

  % update dpgm
%   parfor qq = 1:nparallel  % update dpgm
  for qq = 1:nparallel

    if isintg(qq) && ishopaccept(qq) && isregen(qq)

%         iterafteradapt(qq) = s - lastupdate(qq);
      iterlastregen(qq) = s;

      if samp4adapt(qq) < s - lastupdate(qq) && ~dpgm(qq).uniform  % Need to update the kernel.

        if domixchain

% %           if lencombsamp < nsubsamp
% %             % ixsubsamp =  ceil(rand(1,lencombsamp)*lencombsamp);
% %             ixsubsamp = 1:lencombsamp;
% %           else
% %             % ixsubsamp =  ceil(rand(1,nsubsamp)*lencombsamp);
% %             % thinning
% % %               ixsubsamp = qq:ceil((lencombsamp-qq)/nsubsamp):lencombsamp;
% %             ixsubsamp = round(linspace(1, lencombsamp, nsubsamp));
% %           end

%           dpgm(qq) = dpgaussmixture(CSAMP(:,ixsubsamp),maxncompo,dpgmalgo);
%           dpgm(qq) = dpgaussmixture(CSAMP(:,ixsubsamp),nsubsamp,maxncompo,dpgmalgo,dpgm(qq).uniform,dpgm(qq));
          dpgm(qq) = dpgaussmixture_Yutian(CSAMP,nsubsamp,maxncompo,dpgmalgo,dpgm(qq).uniform,dpgm(qq));

          % update tgradius
          [~,dist_tg_vec] = isinregionL(CSAMP(:,dpgm(qq).ixsub), dpgm(qq).mu, dpgm(qq).L, tgradius(qq));
          dhist = min(dist_tg_vec, [], 1);
          tgradius(qq) = quantile(dhist, trquant);
          
          offset(qq) = getmodeoffset(llhfunc,dpgm(qq),tgradius(qq));
%           maxfreqlogwx(qq) = histwx(CSAMP(:,ixsubsamp)',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);
          maxfreqlogwx(qq) = histwx(CSAMP(:,dpgm(qq).ixsub)',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);

        else

% %           if s < nsubsamp
% %             % ixsubsamp = ceil(rand(1,lencombsamp)*lencombsamp);
% %             ixsubsamp = 1:s;
% %           else
% %             % ixsubsamp = ceil(rand(1,nsubsamp)*lencombsamp);
% %             % thinning
% % %               ixsubsamp = qq:ceil((s-qq)/nsubsamp):s;
% %             ixsubsamp = round(linspace(1, s, nsubsamp));
% %           end

%           dpgm(qq) = dpgaussmixture(SAMP(:,ixsubsamp,qq),maxncompo,dpgmalgo);
%           dpgm(qq) = dpgaussmixture(SAMP(:,ixsubsamp,qq),nsubsamp,maxncompo,dpgmalgo,dpgm(qq).uniform,dpgm(qq));
          dpgm(qq) = dpgaussmixture_Yutian(SAMP(:,1:s,qq),nsubsamp,maxncompo,dpgmalgo,dpgm(qq).uniform,dpgm(qq));
          
          % update tgradius
          [~,dist_tg_vec] = isinregionL(SAMP(:,dpgm(qq).ixsub,qq), dpgm(qq).mu, dpgm(qq).L, tgradius(qq));
          dhist = min(dist_tg_vec, [], 1);
          tgradius(qq) = quantile(dhist, trquant);
          
          offset(qq) = getmodeoffset(llhfunc,dpgm(qq),tgradius(qq));
%           maxfreqlogwx(qq) = histwx(SAMP(:,ixsubsamp,qq)',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);
          maxfreqlogwx(qq) = histwx(SAMP(:,dpgm(qq).ixsub,qq)',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);

        end
        nadapt(qq) = nadapt(qq) + 1;
        lastupdate(qq) = s;
%           iterafteradapt(qq) = 0;
        samp4adapt(qq) = floor(samp4adapt(qq)*samp4adapt_inc);

        sprintf('DPMM adapted! id:%d t:%d, SAMP4ADAPT:%d',qq,t,samp4adapt(qq))

%           % sample independently from Q(.)
%           if REJECPROP % sample from  min{p(x), mf(x)}
% 
%             [wdpgm(:,qq),~,~,~] = indepsampler_dpgm(...
%               wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),2,tgradius(qq));
% 
%           else % sample from f(x) and accept by min{1,w(y)/C}
% 
%             [wdpgm(:,qq),~,~,~] = indepsampler_dpgm(...
%               wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),3,tgradius(qq));
% 
%           end

        % sample independently from Q(.)
        if REJECPROP == 0 % sample from f(x) and accept by min{1,w(y)/C}
          [wdpgm(:,qq),~,~,~,~,numreject_t] = indepsampler_dpgm(...
            wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),3,tgradius(qq));
        elseif REJECPROP == 1 % sample from  min{p(x), mf(x)}
          [wdpgm(:,qq),~,~,~,~,numreject_t] = indepsampler_dpgm(...
            wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),2,tgradius(qq));
        elseif REJECPROP == 4 || REJECPROP == 5
          % do nothing
        else
          error(fprintf('undefined REJECPROP: %d', REJECPROP));
        end

        wcurr(:,qq) = wdpgm(:,qq);
    
        SAMP(:,s,qq) = wcurr(:,qq);
    
      end  % if samp4adapt(qq) < iterafteradapt(qq)

    end  % isintg(qq) && ishopaccept(qq) && isregen(qq)

  end

  isregen = zeros(1,nparallel);

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% save
%   if ~rem(t+itvsave,itvsave) && p > 0
% 
%     v = v + 1;
% 
%     cd 00_log_working

    % PNG
    %                 saveas(fig.hist3(1),strcat(fname,'_HIST1.png'),'png');
    %                 saveas(fig.hist3(2),strcat(fname,'_HIST2.png'),'png');
    %                 saveas(fig.R,strcat(fname,'_R.png'),'png');
    %                 saveas(fig.rerror_mu,strcat(fname,'_REMU.png'),'png');
    %                 saveas(fig.rerror_cov,strcat(fname,'_RECV.png'),'png');
    %
    %                 % FIG
    %                 saveas(fig.hist3(1),strcat(fname,'_HIST1.fig'),'fig');
    %                 saveas(fig.hist3(2),strcat(fname,'_HIST2.fig'),'fig');
    %                 saveas(fig.R,strcat(fname,'_R.fig'),'fig');
    %                 saveas(fig.rerror_mu,strcat(fname,'_REMU.fig'),'fig');
    %                 saveas(fig.rerror_cov,strcat(fname,'_RECV.fig'),'fig');

%     save(fname);
%     % save(fname,'SAMP','s','s_init','seed','dim','lfn','lfsize','nrestart','ninitsamp','nburnin','nsubsamp');
% 
%     disp('---------------- SAVE COMPLETE! ----------------');
%     cd ..
% 
%   end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% print
  if ~rem(t+itvprint,itvprint)

    %                 fname
    % elapsed
    %                 iterafteradapt
    %[ratehopaccept(nadapt(qq)+1) rateregen(nadapt(qq)+1)]
    hmcacceptrate = nhmcaccept./nhmc

    for qq=1:min(2, nparallel)

      sprintf('qq:%d\n',qq)
      sprintf('t:%d, s:%d\nnhmc:%d\nnreject:%d\nnintg:%d\nnhopaccept:%d\nnindepsamp:%d\nnregen:%d\n',...
        t,s,nhmc(qq),nreject(qq),nintg(qq),nhopaccept(qq),nindepsamp(qq),nregen(qq))

    end

  end


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% plot

  if ~rem(t+itvplot,itvplot)

    p = p + 1;
    
    S(p) = s;
    TIME(p) = toc(time_start);

    %% R convergence Diagnostic
    if s > nburnin

      % compute R convergence diagnostic
      RR(p) = mpsrf(permute(SAMP(:,s_init+1:s,:), [2 1 3]));
      
      %% compute error
      for qq=1:nparallel

        sampmu = mean(SAMP(:,s_init+1:s,qq)');
        sampcov = cov(SAMP(:,s_init+1:s,qq)');
        ELOC(p,qq) = sum(abs(ld.Xs(:)' - sampmu));

        type = '-k';
        REMU(p,qq) = sum(abs(gt.mu - sampmu))/sum(abs(gt.mu));
        set(0, 'currentfigure', fig.rerror_mu);
%         plot(1:p, REMU(1:p,qq), type);
        plot(TIME(1:p), REMU(1:p,qq), type); title('REM');
        
        RECV(p,qq) = sum(sum(abs(gt.cov - sampcov)))/sum(sum(abs(gt.cov)));
        set(0, 'currentfigure', fig.rerror_cov);
%         plot(1:p, RECV(1:p,qq), type);
        plot(TIME(1:p), RECV(1:p,qq), type); title('REC');
        
        set(0, 'currentfigure', fig.R);
%         plot(1:p, RR(1:p), type);
        plot(TIME(1:p), RR(1:p), type); title('R');
        
        set(0, 'currentfigure', fig.locerr);
%         plot(1:p, ELOC(1:p,qq) , type);
        plot(TIME(1:p), ELOC(1:p,qq) , type); title('ELOC');

      end

    end

    drawnow;

    %% plot

    for qq=1:nparallel

      set(0,'currentfigure',fig.hist3(qq)); clf

      ss = reshape(SAMP(:,s_init+1:s,qq), [nnode, 2, s-s_init]);
      ss = permute(ss,[2 1 3]);
      ss = ss(:,:);
      [CU,HU] = hist3image(ss(1,:),ss(2,:),70);
      imagesc(CU{1},CU{2},-HU); axis xy; colormap hot; hold on;

      % draw base stations
      for nn = 1:3
        plot(hmc.Xb(nn,1),hmc.Xb(nn,2),'ks','linewidth',2,'markersize',10);
      end

      for kk = 1:dpgm(qq).k

        muk = dpgm(qq).mu(:,kk);
        muk = reshape(muk,[nnode, 2]);
        cvk = dpgm(qq).cv(:,:,kk);

        for nn = 1:nnode

          plot(muk(nn,1), muk(nn,2), 'bo', 'linewidth', 2, 'markersize', 2);
          plotGauss(muk(nn,1), muk(nn,2),...
            cvk(nn,nn),cvk(nn+nnode,nn+nnode),cvk(nn+nnode,nn),...
            2,'--r',1);

        end

      end

      for i = 1:nnode

        plot(ld.Xs(i,1),ld.Xs(i,2),'kx','linewidth',2,'markersize',10);
        plot(wcurr(i,qq),wcurr(i+nnode,qq),'ro','linewidth',2,'markersize',10);

      end

      hold off
      axis([-0.1 1 -0.3 1])
      % axis(axax(ff,:));

      title(strrep(fname,'_','-'));
      drawnow;

    end

    toc
    tic

  end
end

%% save when finished
fname = fname(2:end);
cd 00_log_finished

saveas(fig.hist3(1),strcat(fname,'_HIST1.png'),'png');
% saveas(fig.hist3(2),strcat(fname,'_HIST2.png'),'png');
saveas(fig.R,strcat(fname,'_R.png'),'png');
saveas(fig.rerror_mu,strcat(fname,'_REMU.png'),'png');
% saveas(fig.rerror_cov,strcat(fname,'_RECV.png'),'png');
%
% saveas(fig.hist3(1),strcat(fname,'_HIST1.fig'),'fig');
% saveas(fig.hist3(2),strcat(fname,'_HIST2.fig'),'fig');
% saveas(fig.R,strcat(fname,'_R.fig'),'fig');
% saveas(fig.rerror_mu,strcat(fname,'_REMU.fig'),'fig');
% saveas(fig.rerror_cov,strcat(fname,'_RECV.fig'),'fig');

save(fname);
cd ..
disp('---------------- RUN COMPLETE! ----------------');
% save(fname,'SAMP','s','s_init','seed','dim','lfnum','lfsize','nrestart','ninitsamp','iterburnin','nsubsamp');
