function [llh, grad, met, minfun] = funs_wsn(Xb, Yb, Ys, N, R, sig)

llh = @(ta)wsn_llh(ta, Xb, Yb, Ys, N, R, sig);
grad = @(ta)wsn_grad(ta, Xb, Yb, Ys, N, R, sig);
met = @(ta,opt)wsn_met(ta, Xb, Yb, Ys, N, R, sig, opt);
minfun = @(ta)wsn_minfun(ta, Xb, Yb, Ys, N, R, sig);

end

function f = wsn_llh(ta, Xb, Yb, Ys, N, R, sig)
X = reshape(ta, N, 2);

Obs = [Ys ~= 0, Yb ~= 0];

dx = bsxfun(@minus, X(:,1), [X(:,1)' Xb(:,1)']);
dy = bsxfun(@minus, X(:,2), [X(:,2)' Xb(:,2)']);
dist2 = dx.^2 + dy.^2 + 1e-10;

logPo = -1/(2 * R^2) * dist2  - 1e-10;

ll1 = log(1 - exp(logPo));
ll2 = logPo - 1/2*log(2*pi*sig^2) - 1/(2*sig^2) * (sqrt(dist2) - [Ys, Yb]).^2;

f = sum(sum(triu((1 - Obs) .* ll1 + Obs .* ll2, 1)));
end

function g = wsn_grad(ta, Xb, Yb, Ys, N, R, sig)
X = reshape(ta, N, 2);

Obs = [Ys ~= 0, Yb ~= 0];

dx = bsxfun(@minus, X(:,1), [X(:,1)' Xb(:,1)']);
dy = bsxfun(@minus, X(:,2), [X(:,2)' Xb(:,2)']);
dist2 = dx.^2 + dy.^2 + 1e-10;

logPo = -1/(2 * R^2) * dist2;
Po = exp(logPo);

g1 = Po./(1-Po) / R^2;
g2 = -1 / R^2 - (1 - [Ys, Yb]./sqrt(dist2)) / sig^2;

g1x = g1 .* dx;
g1y = g1 .* dy;
g2x = g2 .* dx;
g2y = g2 .* dy;

gx = sum((1 - Obs) .* g1x + Obs .* g2x, 2) - diag(g1x);
gy = sum((1 - Obs) .* g1y + Obs .* g2y, 2) - diag(g1y);
g = [gx; gy];

end

function [M InvM dM Gamma1] = wsn_met(ta, Xb, Yb, Ys, N, R, sig, opt)
if(nargin<8)
    opt=0;
end

X = reshape(ta(1:end-1), N, 2);
Obs = [Ys ~= 0, Yb ~= 0];

dx = bsxfun(@minus, X(:,1), [X(:,1)' Xb(:,1)']);
dy = bsxfun(@minus, X(:,2), [X(:,2)' Xb(:,2)']);
dist2 = dx.^2 + dy.^2 + 1e-10;

logPo = -1/(2 * R^2) * dist2;
Po = exp(logPo);

Gcom = Po.*(1./dist2./sig^2+1./(1-Po)./R^4);
Gxx = Gcom.*dx.^2;
Gxy = Gcom.*dx.*dy;
Gyy = Gcom.*dy.^2;

M = [diag(sum(Gxx,2))-Gxx(:,1:N), diag(sum(Gxy,2))-Gxy(:,1:N);...
    diag(sum(Gxy,2))-Gxy(:,1:N), diag(sum(Gyy,2))-Gyy(:,1:N)];
M(2*N+1,2*N+1) = 1;

if all(opt==0)
    InvM=NaN; dM=NaN; Gamma1=NaN;
else
    if any(opt==-1)
        InvM = inv(M);
    end
    if any(opt==1)
        
        dM = zeros(repmat(2*N,1,3));
        
        dGm = cell(repmat(2,1,3));
        dGcom = -2.*Po./dist2.^2./sig^2;
        dGm{1,1,1} = dGcom.*dx.^3+2.*Gcom.*dx; dGm{2,2,2} = dGcom.*dy.^3+2.*Gcom.*dy;
        dGm{1,1,2} = dGcom.*dx.^2.*dy; dGm{2,2,1} = dGcom.*dy.^2.*dx;
        dGm{1,2,1} = dGm{1,1,2} + Gcom.*dy; dGm{1,2,2} = dGm{2,2,1} + Gcom.*dx;
        dGm{2,1,1} = dGm{1,2,1}; dGm{2,1,2} = dGm{1,2,2};

        for i=1:2
            for j=1:2
                for k=1:2
                    Vdiag = zeros(repmat(N,1,3)); Fdiag = zeros(repmat(N,1,3));
                    Vdiag(linspace(1,N^3,N)) = sum(dGm{i,j,k},2);
                    Fdiag(bsxfun(@plus, (1:N+1:N^2)',(0:N-1).*N^2)) = dGm{i,j,k}(:,1:N);
                    dM((i-1)*N+(1:N),(j-1)*N+(1:N),(k-1)*N+(1:N)) = Vdiag - Fdiag - permute(Fdiag,[1 3 2]) + permute(Fdiag,[3 2 1]);
                end
            end
        end
        
        dM(2*N+1,2*N+1,2*N+1) = 0;
        
        if any(opt==3)
            Gamma1 = .5*(permute(dM,[1,3,2]) + permute(dM,[3,2,1]) - dM);
        else
            Gamma1=NaN;
        end
    else
        dM=NaN; Gamma1=NaN;
    end
end





end

function [f, g] = wsn_minfun(ta, Xb, Yb, Ys, N, R, sig)

f = -wsn_llh(ta, Xb, Yb, Ys, N, R, sig);
g = -wsn_grad(ta, Xb, Yb, Ys, N, R, sig);

end
