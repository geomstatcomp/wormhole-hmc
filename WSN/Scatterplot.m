% clear
% close all
% addpath('drawdata');
addpath('../00_utils');
dbstop if error

randn('state', 2013);
rand('twister', 2013);

% scatter plot of WSN
ld = load('./00_wsn_data/data_wsn_N8.mat', 'Xs','Xb', 'Yb', 'Ys', 'N', 'R', 'sig');


Algo = {'drmc','wormhole'};
nAlgo = 2;
colors = distinguishable_colors(nAlgo);
styles = {'--','-.','-s','--x'};

files = dir('./result');
nfiles = length(files) - 2;

for Alg=1:nAlgo
    for nn=1:nfiles
        if ~isempty(strfind(files(nn+2).name,Algo{Alg}))
            mc{Alg} = load(strcat('./result/', files(nn+2).name));
        end
    end
end


fig.scatter = figure(200); clf;
set(fig.scatter,'windowstyle','docked');

% colors = {'b','g','r','c','m','y','Orange','Aqua'};
colors = jet(8);
ptschosen = randsample(1:20000,2000);
%% scatter
for i=1:nAlgo
    subplot(1,2,i);
    
    % draw unknown sensors
    for nn = 1:ld.N

        plot(mc{i}.SAMP(nn,ptschosen),mc{i}.SAMP(nn+ld.N,ptschosen),'.','Color',colors(nn,:),'markersize',5); hold on;
        plot(ld.Xs(nn,1),ld.Xs(nn,2),'ko','linewidth',2,'markersize',10); hold on;
        set(gca,'FontSize',15);
        xlabel('x','Fontsize',18); ylabel('y','Fontsize',18);

    end
    
    % draw base stations
    for nn = 1:3
        plot(ld.Xb(nn,1),ld.Xb(nn,2),'rs','linewidth',2,'markersize',10); hold on;
    end
    
%     title(['Samples by ',Algo{i}],'FontSize',18);
    
    hold off
    axis([-0.1 1 -0.3 1]);
    
    
end
subplot(1,2,1); title('RDMC','FontSize',20);
subplot(1,2,2); title('WHMC','FontSize',20);
drawnow;


% xlabel('Seconds','FontSize',15); ylabel('REM','FontSize',15); 
% legend('RDMC','WORMHOLE','FontSize',15,'location','best');
% title('R','FontSize',20); 

