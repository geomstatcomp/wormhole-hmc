function [wxstar, quant90] = histwxY(W,llhfunc,dpgm,offset,tgradius,type)
% W: N x D

numbin = 200;

if type == 1 % batch mode

    logpx = llhfunc(W) + offset;
    logfx = logdpgmpdf(W,dpgm,tgradius);
    logwx = logpx - logfx;

elseif type == 2 % compute isinregion one by one

    idx_isintg = find(any(isinregionL(W',dpgm.mu,dpgm.L,tgradius), 1));
    WW = W(idx_isintg, :);
    
    logpx = zeros(length(idx_isintg), 1);
    for i = 1 : length(idx_isintg)
      logpx(i) = llhfunc(WW(i,:)') + offset;
    end
%     logpx = llhfunc(WW) + offset;
    logfx = logdpgmpdf(WW,dpgm,tgradius);
    logwx = logpx - logfx;   
    
%     j = 0;
%     WW = zeros(0, size(W,2));
%     logpx = zeros(0, 1);
%     for i=1:size(W, 1)
%         wi = W(i,:);
%         if any(isinregionL(wi',dpgm.mu,dpgm.L,tgradius))
%             j = j + 1;
%             WW(j,:) = wi;
%             logpx(j,1) = llhfunc(wi(:)) + offset;
%         end
%     end
% %     logpx = llhfunc(WW) + offset;
%     logfx = logdpgmpdf(WW,dpgm,tgradius);
%     logwx = logpx - logfx;


else
    error('undefined type');
end

fig = figure(223);
set(fig,'windowstyle','docked');
hist(logwx,numbin)

[bin,xout] = hist(logwx,numbin);

if 1
    [~,imax] = max(bin);
    wxstar = xout(imax);
%     wxstar = quantile(logwx, .5);
    quant90 = quantile(logwx, .9);
else
    wxstar = xout*bin'/sum(bin);
end

