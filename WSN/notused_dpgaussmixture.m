function dpgm = dpgaussmixture(X,K,algo)

[dim,N] = size(X);
switch algo
    case 1
        opts = mkopts_avdp();
    case 2
        opts = mkopts_csb(K);
    case 3
        opts = mkopts_cdp(K);
    case 4
        opts = mkopts_bj(K);
    otherwise
        error('undefined algorithm type');
end
opts.get_q_of_z = 1;
result = vdpgm(X,opts);
nk = result.K;
mu = result.hp_posterior.m;
qz = result.q_of_z;
Nk = sum(qz);

%% 
if ~all(Nk)
    nk = nk - sum(Nk==0);
    mu(:,Nk==0) = [];
    qz(:,Nk==0) = [];
    Nk(:,Nk==0) = [];    
end


%%
cv = zeros(dim,dim,nk);
for k=1:nk
    muk =  mu(:,k);
    Y = repmat(sqrt(qz(:,k)),1,dim)'.*(X - repmat(muk,1,N));
    cvk = Y*Y'/Nk(k) + eye(dim)*0.00001;
    cv(:,:,k) = (cvk+cvk')/2;    
end
pik =  Nk/N;
pik = pik/sum(pik);

%% choose top 'K' components
if algo == 1 && K < nk
    nk = K;
    mu = mu(:,1:nk);
    cv = cv(:,:,1:nk);
    pik = pik(1:nk)/sum(pik(1:nk));
end

dpgm.mu = mu;
dpgm.cv = cv;
dpgm.pik = pik;
dpgm.k = nk;
dpgm.result = result;