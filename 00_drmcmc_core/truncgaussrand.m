function prop = truncgaussrand(mu,cv,tr_rad,dim)
% sampling from a stardard Gaussian truncated by a radius, tr_rad.

mu_r = sqrt(dim-1)+0.001;     % compute mean of the Gamma
alpha = 0.5*dim;
beta = 1;

%% radius follows a Gamma distribution on unitv not r
r2u = @(rr) 0.5*rr^2;
u2r = @(uu) sqrt(2*uu);

%% sample unitv vector
unitv = mvnrnd(zeros(dim,1),eye(dim));
unitv = unitv/norm(unitv);

%% sample r
if tr_rad > mu_r 
    
    % sample r from gamrnd()
    r = Inf;
    numreject = -1;
    while r > tr_rad
        numreject = numreject + 1;
        u = gamrnd(alpha,beta);
        r = u2r(u);
    end    
    
else
    % sample r by rejection sampling   
    unifenvel = gampdf(r2u(tr_rad),alpha,beta);
    accept = 0;
    numreject = 0;
    while ~accept

        r = rand*tr_rad;
        r_pdf = gampdf(r2u(r),alpha,beta);

        accept = rand < min(1,exp(log(r_pdf) - log(unifenvel)));

        if ~accept
            numreject = numreject + 1;
        end

    end
end

prop = mu + sqrtm(cv)*r*unitv';
