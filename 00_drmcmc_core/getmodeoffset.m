function offset = getmodeoffset(llhfunc,dpgm,tgradius)

% find max mode
dens = zeros(dpgm.k,1);
for k=1:dpgm.k
    dens(k) = logdpgmpdf(dpgm.mu(:,k)',dpgm,tgradius);
end
[~,imax] = max(dens);
wmax = dpgm.mu(:,imax);

% compute offset
logfxmode = logdpgmpdf(wmax',dpgm,tgradius);
logpxmode = llhfunc(wmax');
offset = logfxmode - logpxmode;
