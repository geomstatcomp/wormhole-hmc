function [wnew,ishopaccept,prhop,isregen,prregen,numreject] = ...
    indepsampler_dpgm(wcurr,llhfunc,dpgm,m,offset,maxfreqlogwx,type,tgradius)

% m : a constant > 1 mutiplied to the proposal distribution to make an envelope function
% type:
%       0=> method1 (no rejection)
%       1 => method2 proposal (reject+mh) ~ min{p(x), mf(x)} + MH 
%       2 => method2 after regen proposal ~ min{p(x), mf(x)}
%       3 => method1 after regen proposal ~ f(y) and accept by min{1,w(y)/C} 

global t

dimw = length(wcurr);
ishopaccept = 0;
prhop = 0;
isregen = 0;
prregen = 0;
numreject = -1;

if type == 1
    logC = 1;
else
    logC = maxfreqlogwx;% - log(2);
end

if type == 0;   m =1;   end


% propose from dpgm
dosample = 1;    

while dosample

    numreject = numreject + 1;
    ixk = find(mnrnd(1,dpgm.pik));  
    wprop = truncgaussrand(dpgm.mu(:,ixk),dpgm.cv(:,:,ixk),tgradius,dimw);        
    logpxprop = llhfunc(wprop') + offset;
    logfxprop = logdpgmpdf(wprop',dpgm,tgradius) + log(m);

    if type == 0 % method 1
        dosample = 0;
    elseif type == 1 || type == 2  %  ~ min{p(x), mf(x)}
        prrejectaccept = min(1,exp(logpxprop-logfxprop));
        dosample = prrejectaccept < 1 && rand > prrejectaccept;
        logfxprop = min(logpxprop, logfxprop);
    elseif type == 3  % ~ min{1,w(y)/C}
        prrejectaccept = min(1,exp(logpxprop-(logfxprop+logC)));
        dosample = rand > prrejectaccept;
    else
        error('undefined type');
    end
end    


if type == 2 || type == 3    
    wnew = wprop; 
    return  % return without MH
else
    % MH Step for type 0 and 1
    % curr
    logpxcurr = llhfunc(wcurr') + offset;
    logfxcurr = logdpgmpdf(wcurr',dpgm,tgradius) + log(m);
    
    if type == 1
        logfxcurr = min(logpxcurr,logfxcurr);
    end

    % compute pr_accept
    logwx = logpxcurr - logfxcurr;
    logwy = logpxprop - logfxprop;
    prhop = min(1,exp(logwy-logwx));

    % test regeneration 
    ishopaccept = rand < prhop;
    prregen = 0;
    if ishopaccept

        wnew = wprop;
        
        if logwx < logC && logwy < logC
                prregen = max(exp(logwx-logC), exp(logwy-logC));                
        elseif logwx > logC && logwy > logC
                % prregen = C*max(1/wx,1/wy);
                prregen = max(exp(logC-logwx), exp(logC-logwy));
        else
                prregen = 1;
        end
        
        prregen2 = min(exp(logC-logwx),1)*min(exp(logwy-logC),1) / prhop;
        
        assert(abs(prregen - prregen2) < 1e-10);
        % adapt if regenerated
        isregen = rand < prregen;            
    else
        isregen = 0;
        wnew = wcurr;
    end
end
