function dpgm = dpgmLaplace(filename)

load(filename, 'Sig', 'mu')

cv = Sig;

[dim, nk] = size(mu);
assert(nk == size(Sig, 3))

L = zeros(dim,dim,nk);
for k=1:nk
    L(:,:,k) = chol(cv(:,:,k), 'lower');
end

pik = zeros(1, nk);
for k = 1 : nk
  pik(k) = sum(log(diag(L(:,:,k))));
end
pik = exp(pik - logsumexp(pik));
pik = pik / sum(pik);

dpgm.mu = mu;
dpgm.cv = cv;
dpgm.L = L;
dpgm.pik = pik;
dpgm.k = nk;
dpgm.uniform = true;
