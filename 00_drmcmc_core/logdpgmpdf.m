function logpdf = logdpgmpdf(W,dpgm,tgradius)

% check if it is in the regeion
j = 0;
WW = zeros(size(W));
ixinregion = zeros(size(W,1),1);
for i=1:size(W,1)
    wi = W(i,:);    
    if any(isinregion(wi,dpgm.mu,dpgm.cv,tgradius))
        j = j + 1;
        ixinregion(j) = 1;
        WW(j,:) = wi;
    end        
end     
WW = WW(1:j,:);

%
[n,dim] = size(WW);
logNk = zeros(n,dpgm.k);
volume = gammainc(tgradius^2 / 2, dim / 2);
% volume = normcdf([-tgradius tgradius]);
% volume = volume(2) - volume(1);

for k=1:dpgm.k    
    logNk(:,k) = logmvnpdf(WW,dpgm.mu(:,k)',dpgm.cv(:,:,k)) - log(volume) + log(dpgm.pik(k));
%     logNk(k) = logtruncgausspdf(w,dpgm.mu(:,k),dpgm.cv(:,:,k),tgradius) ...
%                         + log(dpgm.pik(k));
end
logpdf(logical(ixinregion)) = logsumexp(logNk,2);
logpdf(~logical(ixinregion)) = -Inf;
logpdf = logpdf';