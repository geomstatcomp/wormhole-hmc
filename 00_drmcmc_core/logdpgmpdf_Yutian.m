function logpdf = logdpgmpdf_Yutian(W,dpgm,tgradius)
% W: N x Dim

n = size(W,1);
dim = size(dpgm.mu, 1);
assert(dim == size(W,2))

if ~dpgm.uniform % Truncated Gaussian

  % volume = normcdf([-tgradius tgradius]);
  % volume = volume(2) - volume(1);
  volume = gammainc(tgradius^2 / 2, dim / 2);

  logNk = zeros(n,dpgm.k);
  logNk2 = zeros(n,dpgm.k);
  for k = 1 : dpgm.k
    logNk(:,k) = logmvnpdf(W,dpgm.mu(:,k)',[],dpgm.L(:,:,k)) - log(volume) + log(dpgm.pik(k));
  %     logNk(k) = logtruncgausspdf(w,dpgm.mu(:,k),dpgm.cv(:,:,k),tgradius) ...
  %                         + log(dpgm.pik(k));
  
    logNk2(:,k) = logtgpdf(W,dpgm.mu(:,k)',dpgm.L(:,:,k),tgradius) - log(volume) + log(dpgm.pik(k));
  end
  
  isintg = isinregionL(W',dpgm.mu,dpgm.L,tgradius)'; % N x Dim
  logNk(~isintg) = -inf;

  logpdf = logsumexp(logNk,2);

else % Uniform  
  volumes = zeros(dpgm.k, 1);
  for k = 1 : dpgm.k
    volumes(k) = dim/2 * log(pi) + dim * log(tgradius)...
      + sum(log(diag(dpgm.L(:,:,k)))) - gammaln(1 + dim/2);
  end
  volume = logsumexp(volumes);
  
  isintg = isinregionL(W',dpgm.mu,dpgm.L,tgradius); % Dim x N
  logpdf = log(sum(isintg, 1)') - volume;
end
  
end

function logpdf = logtgpdf(x,mu,L,tgradius)
% x: N x D
% mu: 1 x D
% cv/L: D x D

d = size(x, 2);

x = bsxfun(@minus, x, mu);
xLinv = x / L'; % N x D

logSqrtDetSigma = sum(log(diag(L)));
quadform = sum(xLinv.^2, 2);

logpdf = -0.5*quadform - logSqrtDetSigma - d*log(2*pi)/2;
logpdf(quadform > tgradius^2) = -inf;

end
