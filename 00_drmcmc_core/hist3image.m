function [CU,HU] = hist3image(X,Y,numcell)

[HU,CU] = hist3(squeeze([X; Y]'),[numcell,numcell]);
HU = HU'./sum(HU(:));