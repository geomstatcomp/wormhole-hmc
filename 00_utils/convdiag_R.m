function R = convdiag_R(SAMP,s)

[dim,nsamp,nchain] = size(SAMP);

if nargin == 1
        s = nsamp;
end


for dd = 1:dim

        for qq = 1:nchain

                wvar(qq) = var(SAMP(dd,1:s,qq));                                
                wmu(qq) = mean(SAMP(dd,1:s,qq));

        end                        

        WV(dd) = mean(wvar);
        BV(dd) = nsamp*var(wmu);
        V(dd) = (1-1/s)*WV(dd) + (1/s)*BV(dd);
        R(dd) = sqrt(V(dd)/WV(dd));

end
