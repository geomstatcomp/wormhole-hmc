function logpdf = logmvnpdf(x,mu,cv)

if iscolumn(x)
        x = x';
end

if iscolumn(mu)
        mu = mu';
end


[~,logpdf] = mvnpdf(x,mu,cv);